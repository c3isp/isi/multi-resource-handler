package it.cnr.iit.utils;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.logging.Logger;

public class TimeUtils {

	private static final Logger log = Logger.getLogger(TimeUtils.class.getName());

	private TimeUtils() {
	}

	public static String parseDate(String date) {
		return parseDate(date, "yyyy-MM-dd");

	}

	public static String parseTime(String date) {
		return parseDate(date, "HH:mm:ss");
	}

	private static String parseDate(String date, String format) {
		try {
			ZonedDateTime d = ZonedDateTime.parse(date);
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
			return formatter.format(d);
		} catch (IllegalArgumentException | DateTimeParseException e) {
			log.severe("Error parsing date : " + e.getClass() + " : " + e.getMessage());
			return "";
		}
	}
}
