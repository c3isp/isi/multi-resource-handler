package it.cnr.iit.obligations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.wso2.balana.AbstractPolicy;
import org.wso2.balana.PDPConfig;
import org.wso2.balana.ParsingException;
import org.wso2.balana.Policy;
import org.wso2.balana.PolicyReference;
import org.wso2.balana.PolicySet;
import org.wso2.balana.PolicyTreeElement;
import org.wso2.balana.combine.CombinerElement;
import org.wso2.balana.ctx.AbstractRequestCtx;
import org.wso2.balana.ctx.AbstractResult;
import org.wso2.balana.ctx.EvaluationCtx;
import org.wso2.balana.ctx.EvaluationCtxFactory;
import org.wso2.balana.ctx.RequestCtxFactory;
import org.wso2.balana.ctx.ResponseCtx;
import org.wso2.balana.ctx.ResultFactory;
import org.wso2.balana.ctx.Status;
import org.wso2.balana.ctx.xacml3.RequestCtx;
import org.wso2.balana.ctx.xacml3.Result;
import org.wso2.balana.ctx.xacml3.XACML3EvaluationCtx;
import org.wso2.balana.finder.PolicyFinder;
import org.wso2.balana.finder.PolicyFinderModule;
import org.wso2.balana.finder.PolicyFinderResult;

import it.cnr.iit.ucs.constants.STATUS;
import it.cnr.iit.ucs.pdp.PDPEvaluation;
import it.cnr.iit.ucs.requestmanager.PolicyHelper;
import it.cnr.iit.utility.JAXBUtility;
import it.cnr.iit.utils.ConversionUtils;
import oasis.names.tc.xacml.core.schema.wd_17.PolicyType;
import oasis.names.tc.xacml.core.schema.wd_17.ResponseType;

/**
 * The pdp is basically a wrapper around the one offered by BALANA. <br>
 * In our implementation we are able to evaluate single condition policies only,
 * hence we need the context handler to pass to the PDP only the condition it
 * effectively wants to be evaluated. This because BALANA is designed for XACML
 * that is slightly different than UXACML. In particular, in the former, it is
 * allowed to have only one condition per rule.
 *
 * @author Antonio La Marra, Fabio Bindi, Filippo Lauria, Alessandro Rosetti
 *
 */
public final class PolicyDecisionPoint {

	private static final Logger log = Logger.getLogger(PolicyDecisionPoint.class.getName());

	private PDPConfig pdpConfig;

	public PDPEvaluation evaluate(String request, String policy) {
		return evaluate(request, policy, STATUS.TRY);
	}

	public PDPEvaluation evaluate(String request, String policy, STATUS status) {
		try {
			log.info(() -> "Request [" + ConversionUtils.humanReadableByteCount(request.length(), true) + "], Policy ["
					+ ConversionUtils.humanReadableByteCount(policy.length(), true) + "]");
			log.info(() -> "[Request] : " + request.replace("\n", ""));
			log.info(() -> "[Policy]  : " + policy.replace("\n", ""));
			String conditionName = extractFromStatus(status);
			PolicyHelper policyHelper = PolicyHelper.buildPolicyHelper(policy);
			String policyToEvaluate = policyHelper.getConditionForEvaluation(conditionName);
			PolicyType policyType = JAXBUtility.unmarshalToObject(PolicyType.class, policyToEvaluate);

			HashMap<String, ResponseCtx> responses = new HashMap<>();
			HashMap<String, String> responsesResult = new HashMap<>();
			String policyAsString = JAXBUtility.marshalToString(PolicyType.class, policyType, "Policy",
					JAXBUtility.SCHEMA);
			PolicyFinder policyFinder = new PolicyFinder();
			Set<PolicyFinderModule> policyFinderModules = new HashSet<>();
			InputStreamPolicyFinderModule dataUCONPolicyFinderModule = new InputStreamPolicyFinderModule(
					policyAsString);
			policyFinderModules.add(dataUCONPolicyFinderModule);
			policyFinder.setModules(policyFinderModules);
			policyFinder.init();
			ResponseCtx response = evaluate(request, policyFinder);

			String stringResult = convertResultToString(response.getResults().iterator().next().getDecision());
			ResponseType responseType = JAXBUtility.unmarshalToObject(ResponseType.class, response.encode());
			responses.put(response.encode(), response);
			String ruleId = getRuleId(responseType);
			responsesResult.put(policyType.getPolicyId() + "-" + ruleId, stringResult);
			PDPResponse pdpResponse = new PDPResponse(response);
			log.info(() -> "PDP Decision is "
					+ convertResultToString(response.getResults().iterator().next().getDecision()) + " from ruleId : "
					+ ruleId);
			pdpResponse.setAdditionalInformations(responsesResult.toString());
			return pdpResponse;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getRuleId(ResponseType responseType) {
		try {
			return responseType.getResult().iterator().next().getObligations().getObligation().get(0).getObligationId();
		} catch (Exception e) {
			return "<UNAVAILABLE>";
		}
	}

	private String convertResultToString(int result) {
		result = result > AbstractResult.DECISION_NOT_APPLICABLE ? AbstractResult.DECISION_NOT_APPLICABLE : result;
		return AbstractResult.DECISIONS[result];
	}

	private ResponseCtx evaluate(String request, PolicyFinder policyFinder) {
		try {
			AbstractRequestCtx requestCtx = RequestCtxFactory.getFactory()
					.getRequestCtx(request.replaceAll(">\\s+<", "><"));
			return evaluate(requestCtx, policyFinder);
		} catch (ParsingException e) {
			return getIndeterminate(e.getMessage());
		}
	}

	private ResponseCtx evaluate(AbstractRequestCtx request, PolicyFinder policyFinder) throws ParsingException {
		EvaluationCtx evalContext = EvaluationCtxFactory.getFactory().getEvaluationCtx(request, pdpConfig);
		return new ResponseCtx(evaluateContext(evalContext, policyFinder));
	}

	private ResponseCtx getIndeterminate(String message) {
		String error = "Error : " + message;
		ArrayList<String> code = new ArrayList<>();
		code.add(Status.STATUS_SYNTAX_ERROR);
		Status status = new Status(code, error);
		return new ResponseCtx(new Result(AbstractResult.DECISION_INDETERMINATE, status));
	}

	private AbstractResult evaluateContext(EvaluationCtx context, PolicyFinder policyFinder) {
		PolicyFinderResult finderResult = policyFinder.findPolicy(context);
		if (finderResult.notApplicable()) {
			return ResultFactory.getFactory().getResult(AbstractResult.DECISION_NOT_APPLICABLE, context);
		} else if (finderResult.indeterminate()) {
			return ResultFactory.getFactory().getResult(AbstractResult.DECISION_INDETERMINATE, finderResult.getStatus(),
					context);
		} else if (context instanceof XACML3EvaluationCtx
				&& ((RequestCtx) context.getRequestCtx()).isReturnPolicyIdList()) {
			Set<PolicyReference> references = new HashSet<>();
			processPolicyReferences(finderResult.getPolicy(), references);
			((XACML3EvaluationCtx) context).setPolicyReferences(references);
		}
		return finderResult.getPolicy().evaluate(context);
	}

	private void processPolicyReferences(AbstractPolicy policy, Set<PolicyReference> references) {
		if (policy instanceof Policy) {
			references.add(new PolicyReference(policy.getId(), PolicyReference.POLICY_REFERENCE, null, null));
		} else if (policy instanceof PolicySet) {
			List<CombinerElement> elements = policy.getChildElements();
			if (elements != null && !elements.isEmpty()) {
				for (CombinerElement element : elements) {
					PolicyTreeElement treeElement = element.getElement();
					if (treeElement instanceof AbstractPolicy) {
						processPolicyReferences((AbstractPolicy) treeElement, references);
					} else {
						references.add(
								new PolicyReference(policy.getId(), PolicyReference.POLICYSET_REFERENCE, null, null));
					}
				}
			}
		}
	}

	private String extractFromStatus(STATUS status) {
		switch (status) {
		case TRY:
			return "pre";
		case START:
//		case REEVALUATION:
		case REVOKE:
			return "ongoing";
		case END:
			return "post";
		}
		return null;
	}

}
