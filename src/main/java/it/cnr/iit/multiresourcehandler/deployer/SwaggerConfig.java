package it.cnr.iit.multiresourcehandler.deployer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.info.BuildProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.BasicAuth;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	public static final String URL_PATH = "(?!/error.*).*";

	@Value("${security.activation.status}")
	private boolean securityActivationStatus;

	@Autowired
	BuildProperties buildProperties;

	private SecurityContext securityContext() {
		return SecurityContext.builder().securityReferences(getDefaultAuth()).forPaths(PathSelectors.regex(URL_PATH))
				.build();
	}

	List<SecurityReference> getDefaultAuth() {
		AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
		AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
		authorizationScopes[0] = authorizationScope;
		return new ArrayList<>(Arrays.asList(new SecurityReference("basicAuth", authorizationScopes)));
	}

	@Bean
	public Docket getDocumentation() {
		Docket docket = new Docket(DocumentationType.SWAGGER_2);
		docket.apiInfo(getApiInfo());

		if (!securityActivationStatus) {
			return docket.select().paths(PathSelectors.regex(URL_PATH)).build();
		} else {
			return docket.securitySchemes(new ArrayList<>(Arrays.asList(new BasicAuth("basicAuth"))))
					.securityContexts(new ArrayList<>(Arrays.asList(securityContext()))).select()
					.paths(PathSelectors.regex(URL_PATH)).build();
		}
	}

	private ApiInfo getApiInfo() {
		return new ApiInfoBuilder().title("Multi Resource Handler").description("REST API for Multi Resource Handler Operations")
				.version(buildProperties.getVersion())
				.contact(new Contact("Alessandro Rosetti", "", "alessandro.rosetti@iit.cnr.it")).build();
	}

}
