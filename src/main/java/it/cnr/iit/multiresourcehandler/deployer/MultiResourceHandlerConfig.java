package it.cnr.iit.multiresourcehandler.deployer;

import java.time.Duration;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import it.cnr.iit.clients.dpos.DposClient;
import it.cnr.iit.multiresourcehandler.XacmlUtils;

@Configuration
public class MultiResourceHandlerConfig {

	@Value("${security.user.name}")
	private String restUser;
	@Value("${security.user.password}")
	private String restPassword;

	@Autowired
	private DposClient dposClient;

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
		return restTemplateBuilder.basicAuthentication(restUser, restPassword).setConnectTimeout(Duration.ofSeconds(30))
				.setReadTimeout(Duration.ofSeconds(30)).build();
	}

	@PostConstruct
	public void init() {
		XacmlUtils.setDposUtils(dposClient); // sorry for this ugly hack
	}

}
