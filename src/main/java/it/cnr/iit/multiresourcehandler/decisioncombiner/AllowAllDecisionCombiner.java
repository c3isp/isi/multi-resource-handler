package it.cnr.iit.multiresourcehandler.decisioncombiner;

import java.util.List;

public class AllowAllDecisionCombiner<T> implements DecisionCombiner<T> {

	@Override
	public List<T> getAllowed(List<T> list) {
		return list;
	}

}
