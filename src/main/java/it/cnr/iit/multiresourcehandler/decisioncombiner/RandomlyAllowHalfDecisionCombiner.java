package it.cnr.iit.multiresourcehandler.decisioncombiner;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class RandomlyAllowHalfDecisionCombiner<T> implements DecisionCombiner<T> {

	@Override
	public List<T> getAllowed(List<T> dpoInfoList) {
		return selectHalfElementsRandomly(dpoInfoList);
	}

	public static <T> List<T> selectHalfElementsRandomly(List<T> list) {
		Collections.shuffle(list);
		int size = (int) Math.ceil(list.size() / 2.0);
		return list.stream().limit(size).collect(Collectors.toList());
	}
}
