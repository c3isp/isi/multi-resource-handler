package it.cnr.iit.multiresourcehandler.decisioncombiner;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.cnr.iit.clients.dpos.DpoMetadata;
import it.cnr.iit.multiresourcehandler.ComponentsWrapper;
import it.cnr.iit.multiresourcehandler.UcsUtils;
import it.cnr.iit.multiresourcehandler.db.DpoInfo;
import it.cnr.iit.ucs.constants.STATUS;
import it.cnr.iit.ucs.pdp.PDPEvaluation;
import it.cnr.iit.xacml.wrappers.PolicyWrapper;
import it.cnr.iit.xacml.wrappers.RequestWrapper;

@Component
public class MaximumSetDecisionCombiner implements DecisionCombiner<DpoInfo> {
	private static final Logger log = Logger.getLogger(MaximumSetDecisionCombiner.class.getName());

	@Autowired
	private ComponentsWrapper componentsWrapper;

	@Override
	public List<DpoInfo> getAllowed(List<DpoInfo> dpoInfoList) {
		log.severe("Checking cohabitance");
		boolean[][] conflictMatrix = generateConflictMatrix(dpoInfoList);
		printMatrix(conflictMatrix);
		return selectGreatestListWithoutConflicts(dpoInfoList, conflictMatrix);
	}

	private boolean[][] generateConflictMatrix(List<DpoInfo> dpoInfoList) {
		boolean[][] conflictMatrix = new boolean[dpoInfoList.size()][dpoInfoList.size()];
		for (int i = 0; i < dpoInfoList.size(); i++) {
			for (int j = 0; j < dpoInfoList.size(); j++) {
				if (checkSingleConflict(dpoInfoList.get(i), dpoInfoList.get(j))) {
					conflictMatrix[i][j] = true;
					conflictMatrix[j][i] = true;
				}
			}
		}
		return conflictMatrix;
	}

	public List<DpoInfo> selectGreatestListWithoutConflicts(List<DpoInfo> dpoInfoList, boolean[][] conflictMatrix) {
		int row = 0;
		int cohabitance = 1;
		for (int i = 0; i < dpoInfoList.size(); i++) {
			int currentCohabitance = countRowCohabitance(conflictMatrix[i]);
			if (cohabitance < currentCohabitance) {
				cohabitance = currentCohabitance;
				row = i;
			}
		}
		return translateRow(dpoInfoList, conflictMatrix[row]);
	}

	private int countRowCohabitance(boolean[] conflictRow) {
		return IntStream.range(0, conflictRow.length).mapToObj(i -> conflictRow[i]).map(e -> e ? 0 : 1).reduce(0,
				Integer::sum);
	}

	private List<DpoInfo> translateRow(List<DpoInfo> dpoInfoList, boolean[] conflictRow) {
		List<DpoInfo> rowSet = new ArrayList<>();
		for (int i = 0; i < dpoInfoList.size(); i++) {
			if (!conflictRow[i]) {
				rowSet.add(dpoInfoList.get(i));
			}
		}
		return rowSet;
	}

	private boolean checkSingleConflict(DpoInfo dpoInfo1, DpoInfo dpoInfo2) {
		log.severe("Checking cohabitance of " + dpoInfo1.getDpoId() + " <-> " + dpoInfo2.getDpoId());

		if (dpoInfo1.getDpoId().equals(dpoInfo2.getDpoId())) {
			return false;
		}

		DpoMetadata dpoMetadata = dpoInfo1.getDpoMetadata();
		if (dpoMetadata == null || dpoMetadata.getDsaId().isEmpty()) {
			log.info("Invalid dpoMetadata!");
			return true;
		}

		try {
			String policy = UcsUtils.preparePolicyForCohabitation(
					componentsWrapper.getDsaMapper().getOtherDataPolicy(dpoMetadata.getDsaId()).orElse(""));
			String request = UcsUtils.readRequest(dpoInfo2.getRequestPath());
			String fattenRequest = componentsWrapper.getUcs().enrichRequest(request);

			PDPEvaluation response = componentsWrapper.getPdp().evaluate(RequestWrapper.build(fattenRequest),
					PolicyWrapper.build(policy), STATUS.TRY);

			log.severe("checkCohabitation result: " + response.getResult());

			return !response.getResult().equalsIgnoreCase("permit");
		} catch (Exception e) {
			log.severe("Error checking cohabitance : " + e.getClass() + " " + e.getMessage());
			e.printStackTrace();
			return true;
		}
	}

	private void printMatrix(boolean[][] matrix) {
		for (boolean[] i : matrix) {
			for (boolean element : i) {
				System.out.print((element ? 1 : 0) + " ");
			}
			System.out.println();
		}
	}
}
