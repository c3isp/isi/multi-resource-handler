package it.cnr.iit.multiresourcehandler.decisioncombiner;

import java.util.List;

public interface DecisionCombiner<T> {

	List<T> getAllowed(List<T> list);

}
