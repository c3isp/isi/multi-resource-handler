package it.cnr.iit.multiresourcehandler;

import java.util.Collections;
import java.util.List;

class MultiResponseElement {

	private String dpoID;
	private String status;
	private String response = "";

	private List<String> obligationsRuleIds;

	public MultiResponseElement(String dpoId, String status) {
		obligationsRuleIds = Collections.emptyList();
		setDpoID(dpoId);
		setStatus(status);
	}

	public String getDpoID() {
		return dpoID;
	}

	public void setDpoID(String dpoID) {
		this.dpoID = dpoID;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public List<String> getObligationsRuleIds() {
		return obligationsRuleIds;
	}

	public void setObligationsRuleIds(List<String> obligationsRuleIds) {
		this.obligationsRuleIds = obligationsRuleIds;
	}

}
