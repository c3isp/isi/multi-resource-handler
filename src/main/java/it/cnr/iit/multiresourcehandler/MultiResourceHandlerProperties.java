package it.cnr.iit.multiresourcehandler;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MultiResourceHandlerProperties {

	@Value("${decision-combiner.type:default}")
	private String decisionCombinerType;

	@Value("${ucs.timeout:15}")
	private int timeout;

	public void setDecisionCombinerType(String decisionCombinerType) {
		this.decisionCombinerType = decisionCombinerType;
	}

	public String getDecisionCombinerType() {
		return decisionCombinerType;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

}
