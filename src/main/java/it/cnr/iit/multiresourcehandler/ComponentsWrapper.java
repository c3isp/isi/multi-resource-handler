package it.cnr.iit.multiresourcehandler;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.cnr.iit.clients.dpos.DposClient;
import it.cnr.iit.clients.dsamapper.DsaMapperClient;
import it.cnr.iit.clients.eventhandler.EventHandlerClient;
import it.cnr.iit.clients.ucs.PEP;
import it.cnr.iit.clients.ucs.UcsClient;
import it.cnr.iit.multiresourcehandler.db.DpoInfo;
import it.cnr.iit.multiresourcehandler.db.DpoInfoStorage;
import it.cnr.iit.multiresourcehandler.db.EndAccessStorage;
import it.cnr.iit.multiresourcehandler.decisioncombiner.DecisionCombiner;
import it.cnr.iit.ucs.core.UCSCoreServiceBuilder;
import it.cnr.iit.ucs.pdp.PolicyDecisionPoint;
import it.cnr.iit.ucs.properties.UCSProperties;
import it.cnr.iit.ucs.ucs.UCSInterface;

@Component
public class ComponentsWrapper {

	@Autowired
	private MultiResourceHandler multiResourceHandler;

	@Autowired
	private DpoInfoStorage dpoInfoStorage;

	@Autowired
	private DecisionCombiner<DpoInfo> decisionCombiner;

	@Autowired
	private UcsClient ucsClient;

	@Autowired
	private DposClient dpos;

	@Autowired
	private EventHandlerClient eventHandler;

	@Autowired
	private EndAccessStorage endAccessStorage;

	@Autowired
	private DsaMapperClient dsaMapper;

	@Autowired
	private UCSProperties properties;

	private PEP pep;

	private UCSInterface ucs;

	private PolicyDecisionPoint pdp;

	@PostConstruct
	private void init() {
		pep = new PEP(properties.getPepList().get(0), this);
		ucs = new UCSCoreServiceBuilder(pep, pep.getProperties()).setProperties(properties).build();
		pdp = new PolicyDecisionPoint(properties.getPolicyDecisionPoint());

	}

	public MultiResourceHandler getMultiResourceHandler() {
		return multiResourceHandler;
	}

	public void setMultiResourceHandler(MultiResourceHandler multiResourceHandler) {
		this.multiResourceHandler = multiResourceHandler;
	}

	public DpoInfoStorage getDpoInfoStorage() {
		return dpoInfoStorage;
	}

	public void setDpoInfoStorage(DpoInfoStorage dpoInfoStorage) {
		this.dpoInfoStorage = dpoInfoStorage;
	}

	public DecisionCombiner<DpoInfo> getDecisionCombiner() {
		return decisionCombiner;
	}

	public void setDecisionCombiner(DecisionCombiner<DpoInfo> decisionCombiner) {
		this.decisionCombiner = decisionCombiner;
	}

	public UcsClient getUcsClient() {
		return ucsClient;
	}

	public void setUcsClient(UcsClient ucsClient) {
		this.ucsClient = ucsClient;
	}

	public DposClient getDpos() {
		return dpos;
	}

	public void setDpos(DposClient dpos) {
		this.dpos = dpos;
	}

	public EventHandlerClient getEventHandler() {
		return eventHandler;
	}

	public void setEventHandler(EventHandlerClient eventHandler) {
		this.eventHandler = eventHandler;
	}

	public EndAccessStorage getEndAccessStorage() {
		return endAccessStorage;
	}

	public void setEndAccessStorage(EndAccessStorage endAccessStorage) {
		this.endAccessStorage = endAccessStorage;
	}

	public UCSProperties getProperties() {
		return properties;
	}

	public void setProperties(UCSProperties properties) {
		this.properties = properties;
	}

	public UCSInterface getUcs() {
		return ucs;
	}

	public void setUcs(UCSInterface ucs) {
		this.ucs = ucs;
	}

	public PolicyDecisionPoint getPdp() {
		return pdp;
	}

	public void setPdp(PolicyDecisionPoint pdp) {
		this.pdp = pdp;
	}

	public DsaMapperClient getDsaMapper() {
		return dsaMapper;
	}

	public void setDsaMapper(DsaMapperClient dsaMapper) {
		this.dsaMapper = dsaMapper;
	}

	public PEP getPep() {
		return pep;
	}

	public void setPep(PEP pep) {
		this.pep = pep;
	}

}
