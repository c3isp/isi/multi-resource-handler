package it.cnr.iit.multiresourcehandler;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import it.cnr.iit.clients.dpos.DpoMetadata;
import it.cnr.iit.common.eventhandler.events.Event;
import it.cnr.iit.common.eventhandler.events.multiresourcehandler.MultiResourceHandlerEvent;
import it.cnr.iit.common.eventhandler.events.multiresourcehandler.MultiResourceHandlerEventConstants;
import it.cnr.iit.common.eventhandler.events.multiresourcehandler.MultiResourceHandlerEventTypes;
import it.cnr.iit.common.types.DataManipulationObject;
import it.cnr.iit.multiresourcehandler.db.DPO_STATUS;
import it.cnr.iit.multiresourcehandler.db.DpoInfo;
import it.cnr.iit.multiresourcehandler.db.EndAccessSessions;
import it.cnr.iit.ucs.message.endaccess.EndAccessResponseMessage;
import it.cnr.iit.ucs.message.startaccess.StartAccessResponseMessage;
import it.cnr.iit.ucs.message.tryaccess.TryAccessResponseMessage;
import oasis.names.tc.xacml.core.schema.wd_17.DecisionType;

/**
 * This is the class implementing the translation of messages from the format
 * used by the EventHandler to the one used by the UsageControl.
 *
 * @author Antonio La Marra, Alessandro Rosetti
 */
@Component
public class MultiResourceHandler {

	@Autowired
	private ComponentsWrapper componentsWrapper;

	private static final Logger log = Logger.getLogger(MultiResourceHandler.class.getName());
	private static final Gson gson = new GsonBuilder().create();

	public void handleEvent(MultiResourceHandlerEvent event) {
		if (event.isEventType(MultiResourceHandlerEventTypes.TRY_ACCESS)) {
			tryAccessSingle(event);
		} else if (event.getSessionID() == "111") {
			checkAvailable(event);
		} else if (event.isEventType(MultiResourceHandlerEventTypes.TRY_ACCESS_MULTI)) {
			tryAccessMulti(event);
		} else if (event.isEventType(MultiResourceHandlerEventTypes.END_ACCESS)) {
			endAccess(event);
		} else {
			log.info(() -> "EventType \"" + event.getEventType() + "\"  can't be handled by mrh.");
		}
	}

	private TryAccessResponseMessage tryAccess(Map<String, String> contextMap) {
		log.severe("tryAccess()");
		String request = UcsUtils.patternToLower(contextMap.get(UcsUtils.REQUEST_KEY), "Invoke\\w+");
		String policy = UcsUtils.preparePolicyForAuthorization(contextMap.get(UcsUtils.POLICY_KEY));
		TryAccessResponseMessage response = componentsWrapper.getUcsClient().tryAccess(request, policy);

		// TODO: maybe even if the response is deny?
		String outcome = response.getEvaluation().isDecision(DecisionType.PERMIT) ? "true" : "false";
		List<DataManipulationObject> dmos = null;
		if (outcome.equals("true")) {
			List<String> obligationRuleIdList = getObligationRuleIdList(contextMap.get(UcsUtils.REQUEST_KEY),
					contextMap.get(UcsUtils.POLICY_KEY));
			String obligations = gson.toJson(obligationRuleIdList);
			if (!obligations.equals("[]")) {
				dmos = UcsUtils.performObligation(contextMap.get(UcsUtils.POLICY_KEY), obligations);
			}
		}

		if (contextMap.get(UcsUtils.SESSION_ID_KEY).equals(UcsUtils.SESSION_ID_DEFAULT_VALUE)) {
			tryAccessSingleSuccess(contextMap, response, dmos);
		}
		return response;
	}

	private void tryAccessSingle(MultiResourceHandlerEvent event) {
		Map<String, String> contextMap = createContextMap(event);
		tryAccess(contextMap);
	}

	private void tryAccessSingleSuccess(Map<String, String> contextMap, TryAccessResponseMessage message,
			List<DataManipulationObject> dmos) {
		MultiResourceHandlerEvent responseEvent = createEvent(contextMap,
				MultiResourceHandlerEventTypes.TRY_ACCESS_RESPONSE);

		if (dmos != null) {
			int count = 0;
			for (DataManipulationObject dmo : dmos) {
				responseEvent.getAdditionalProperties().put("dmo" + count,
						dmo.getName() + "," + dmo.getParam() + "," + dmo.getOptions());
				count++;
			}
		}

		responseEvent.setProperty(UcsUtils.RESPONSE, message.getEvaluation().getResult());
		responseEvent.setProperty(UcsUtils.LONG_RESPONSE, message.getEvaluation().getResponse());

		componentsWrapper.getEventHandler().send(responseEvent);
	}

	private void tryAccessMulti(MultiResourceHandlerEvent event) {
		componentsWrapper.getDpoInfoStorage().deleteAllSessionIdEh(event.getSessionID());

		String[] policies = event.getPolicyPaths();
		String[] requests = event.getRequestPaths();

		if (policies == null || requests == null || policies.length != requests.length || policies.length == 0) {
			log.severe("Invalid policies/requests");
			sendMultiResponseEvent(event.getMessageID(), event.getSessionID());
			return;
		}

		List<Map<String, String>> contextMapList = IntStream.range(0, policies.length)
				.mapToObj(i -> createContextMap(event, requests[i], policies[i])).collect(Collectors.toList());

		List<DpoInfo> dpoInfoList = contextMapList.stream().map(this::createDpoInfo).collect(Collectors.toList());
		dpoInfoList.stream().forEach(componentsWrapper.getDpoInfoStorage()::createOrUpdate);

		for (int i = 0; i < policies.length; i++) {
			log.log(Level.INFO, "tryAccessMulti [{0}/{1}]", new Object[] { i + 1, policies.length });
			DpoInfo dpoInfo = dpoInfoList.get(i);
			TryAccessResponseMessage response = tryAccess(contextMapList.get(i));
			tryAccessMultiSuccess(dpoInfo, response);
		}
	}

	public void checkAvailable(MultiResourceHandlerEvent event) {
		componentsWrapper.getDpoInfoStorage().deleteAllSessionIdEh(event.getSessionID());

		String[] policies = event.getPolicyPaths();
		String[] requests = event.getRequestPaths();

		if (policies == null || requests == null || policies.length != requests.length || policies.length == 0) {
			log.severe("Invalid policies/requests");
			sendMultiResponseEvent(event.getMessageID(), event.getSessionID());
			return;
		}

		List<Map<String, String>> contextMapList = IntStream.range(0, policies.length)
				.mapToObj(i -> createContextMap(event, requests[i], policies[i])).collect(Collectors.toList());

		List<DpoInfo> dpoInfoList = contextMapList.stream().map(this::createDpoInfo).collect(Collectors.toList());
		dpoInfoList.stream().forEach(componentsWrapper.getDpoInfoStorage()::createOrUpdate);

		for (int i = 0; i < policies.length; i++) {
			log.log(Level.INFO, "tryAccessMulti [{0}/{1}]", new Object[] { i + 1, policies.length });
			DpoInfo dpoInfo = dpoInfoList.get(i);
			TryAccessResponseMessage response = tryAccess(contextMapList.get(i));
			DPO_STATUS status = response.getEvaluation().isDecision(DecisionType.PERMIT) ? DPO_STATUS.TRYACCESS_PERMIT
					: DPO_STATUS.TRYACCESS_DENY;
			dpoInfo.setStatus(status);
			dpoInfo.setSessionIdUcs(response.getSessionId());
			componentsWrapper.getDpoInfoStorage().createOrUpdate(dpoInfo);
			sendMultiResponseEvent(dpoInfo.getMessageIdEh(), dpoInfo.getSessionIdEh());
		}
	}

	private DpoInfo createDpoInfo(Map<String, String> contextMap) {
		DpoInfo dpoInfo = new DpoInfo();
		dpoInfo.setDpoId(contextMap.get(UcsUtils.DPO_ID_KEY));
		DpoMetadata dpoMetadata = componentsWrapper.getDpos().getDpoMetadata(dpoInfo.getDpoId()).orElse(null);
		dpoInfo.setDpoMetadata(dpoMetadata);
		dpoInfo.setPolicyPath(contextMap.get(UcsUtils.POLICY_PATH_KEY));
		dpoInfo.setRequestPath(contextMap.get(UcsUtils.REQUEST_PATH_KEY));
		dpoInfo.setSessionIdEh(contextMap.get(UcsUtils.SESSION_ID_KEY));
		dpoInfo.setMessageIdEh(contextMap.get(UcsUtils.MESSAGE_ID_KEY));
		dpoInfo.setRequestId(contextMap.get(MultiResourceHandlerEventConstants.REQUEST_ID));
		dpoInfo.setStatus(DPO_STATUS.TRYACCESS_PENDING);

//		try {
//			log.severe("created new entry for dpoInfo: " + new ObjectMapper().writeValueAsString(dpoInfo));
//		} catch (JsonProcessingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		return dpoInfo;
	}

//	private void tryAccessMultiError(DpoInfo dpoInfo, Throwable throwable) {
//		log.info("tryAccessMultiError : " + dpoInfo.getDpoId());
//		dpoInfo.setStatus(DPO_STATUS.ERROR);
//		dpoInfoStorage.createOrUpdate(dpoInfo);
//		tryAccessMultiEnd(dpoInfo.getMessageIdEh(), dpoInfo.getSessionIdEh());
//	}

	private void tryAccessMultiSuccess(DpoInfo dpoInfo, TryAccessResponseMessage message) {
		DPO_STATUS status = message.getEvaluation().isDecision(DecisionType.PERMIT) ? DPO_STATUS.TRYACCESS_PERMIT
				: DPO_STATUS.TRYACCESS_DENY;
		dpoInfo.setStatus(status);
		dpoInfo.setSessionIdUcs(message.getSessionId());
		componentsWrapper.getDpoInfoStorage().createOrUpdate(dpoInfo);
		tryAccessMultiEnd(dpoInfo.getMessageIdEh(), dpoInfo.getSessionIdEh());
	}

	private void tryAccessMultiEnd(String messageIdEh, String sessionIdEh) {
		if (isTryAccessMultiComplete(sessionIdEh)) {
			log.severe("tryAccessMultiEnd : completed!");
			if (dpoInfoStatusCount(sessionIdEh, DPO_STATUS.TRYACCESS_PERMIT) > 0) {
				checkCohabitation(sessionIdEh);
				startAccessMulti(messageIdEh, sessionIdEh);
			} else {
				sendMultiResponseEvent(messageIdEh, sessionIdEh);
			}
		}
	}

	public void checkCohabitation(String sessionIdEh) {
		List<DpoInfo> dpoInfoList = componentsWrapper.getDpoInfoStorage().getForSessionIdEhAndStatus(sessionIdEh,
				DPO_STATUS.TRYACCESS_PERMIT);
		List<DpoInfo> allowedList = componentsWrapper.getDecisionCombiner().getAllowed(dpoInfoList);

		allowedList.stream().forEach(d -> d.setStatus(DPO_STATUS.COHABITATION_ALLOWED));

		List<DpoInfo> rejectedList = new ArrayList<>(dpoInfoList);
		log.severe("Allowed List: " + allowedList.size());
		rejectedList.removeAll(allowedList);
		log.severe("Rejected List: " + rejectedList.size());
		rejectedList.forEach(d -> {
			componentsWrapper.getDpoInfoStorage().delete(d);
			d.setStatus(DPO_STATUS.COHABITATION_REJECTED);
			EndAccessResponseMessage message = sendEndAccess(d);
			deleteEntryFromEndAccessSessions(message.getMessageId());
		});
		dpoInfoList.stream().forEach(d -> componentsWrapper.getDpoInfoStorage().createOrUpdate(d));
		log.severe(() -> "checkCohabitation allowed result : [" + allowedList.size() + "/" + dpoInfoList.size() + "]");
	}

	public void startAccessMulti(String messageIdEh, String sessionIdEh) {
		log.severe("startAccessMulti");
		List<DpoInfo> dpoInfoList = componentsWrapper.getDpoInfoStorage().getForSessionIdEhAndStatus(sessionIdEh,
				DPO_STATUS.COHABITATION_ALLOWED);

		if (!dpoInfoList.isEmpty()) {
			dpoInfoList.stream().forEach(d -> d.setStatus(DPO_STATUS.STARTACCESS_PENDING));
			dpoInfoList.stream().forEach(componentsWrapper.getDpoInfoStorage()::createOrUpdate);
			for (int i = 0; i < dpoInfoList.size(); i++) {
				log.log(Level.SEVERE, "startAccessMulti [{0}/{1}]", new Object[] { i + 1, dpoInfoList.size() });
				DpoInfo dpoInfo = dpoInfoList.get(i);
				StartAccessResponseMessage response = startAccess(dpoInfo);
				startAccessMultiSuccess(dpoInfo, response);
			}
		} else {
			sendMultiResponseEvent(messageIdEh, sessionIdEh);
		}
	}

	private StartAccessResponseMessage startAccess(DpoInfo dpoInfo) {
		log.severe("startAccess : " + dpoInfo.getDpoId());
		StartAccessResponseMessage response = componentsWrapper.getUcsClient().startAccess(dpoInfo.getSessionIdUcs());

		// TODO: maybe even if the response is deny?
		// TODO: test this feature
		String outcome = response.getEvaluation().isDecision(DecisionType.PERMIT) ? "true" : "false";
		if (outcome.equals("true")) {
			MultiResourceHandlerEvent event = new MultiResourceHandlerEvent();
			event.setSessionID(dpoInfo.getSessionIdUcs());
			event.setMessageID(dpoInfo.getMessageIdEh());
			Map<String, String> contextMap = createContextMap(event, dpoInfo.getRequestPath(), dpoInfo.getPolicyPath());
			// how to retrieve policy here?
			List<String> obligationRuleIdList = getObligationRuleIdList(contextMap.get(UcsUtils.REQUEST_KEY),
					contextMap.get(UcsUtils.POLICY_KEY));
			String obligations = gson.toJson(obligationRuleIdList);
			if (!obligations.equals("[]")) {
				log.severe("Performing the following obligation after create: " + obligations);
				List<DataManipulationObject> dmos = UcsUtils.performObligation(contextMap.get(UcsUtils.POLICY_KEY),
						obligations);
			}
		}
		return response;
	}

//	private void startAccessMultiError(DpoInfo dpoInfo, Throwable throwable) {
//		log.info("startAccessMultiError : " + dpoInfo.getDpoId());
//		dpoInfo.setStatus(DPO_STATUS.ERROR);
//		dpoInfoStorage.createOrUpdate(dpoInfo);
//		startAccessMultiEnd(dpoInfo.getMessageIdEh(), dpoInfo.getSessionIdEh());
//	}

	private void startAccessMultiSuccess(DpoInfo dpoInfo, StartAccessResponseMessage message) {
		DPO_STATUS status = message.getEvaluation().isDecision(DecisionType.PERMIT) ? DPO_STATUS.STARTACCESS_PERMIT
				: DPO_STATUS.STARTACCESS_DENY;
		dpoInfo.setStatus(status);
		componentsWrapper.getDpoInfoStorage().createOrUpdate(dpoInfo);
		startAccessMultiEnd(dpoInfo.getMessageIdEh(), dpoInfo.getSessionIdEh());
	}

	private void startAccessMultiEnd(String messageIdEh, String sessionIdEh) {
		if (isStartAccessMultiComplete(sessionIdEh)) {
			log.severe("startAccessMultiEnd");
			sendMultiResponseEvent(messageIdEh, sessionIdEh);
		}
	}

	private Optional<String> createMultiResponseFile(String sessionIdEh) {
		List<DpoInfo> dpoInfoList = componentsWrapper.getDpoInfoStorage().getForSessionIdEh(sessionIdEh);
		List<MultiResponseElement> multiResList = dpoInfoList.stream().map(this::getMultiResponseElement)
				.collect(Collectors.toList());
//		log.severe(() -> "MultiResponse file contents : " + gson.toJson(multiResList));
		if (!dpoInfoList.isEmpty()) {
			String basePath = Paths.get(dpoInfoList.get(0).getPolicyPath()).getParent().toString();
			String path = getMultiResponseFilePath(basePath, sessionIdEh);
			try (FileOutputStream fos = new FileOutputStream(path)) {
				fos.write(gson.toJson(multiResList).getBytes());
				return Optional.of(path);
			} catch (IOException e) {
				log.info("Unable to write response to " + path);
			}
		}
		return Optional.empty();
	}

	private String getMultiResponseFilePath(String basePath, String sessionIdEh) {
		return Paths.get(basePath, "multi-response-" + sessionIdEh + ".json").toString();
	}

	private MultiResponseElement getMultiResponseElement(DpoInfo dpoInfo) {
		MultiResponseElement mre = new MultiResponseElement(dpoInfo.getDpoId(), dpoInfo.getStatus());
		if (dpoInfo.isStatus(DPO_STATUS.STARTACCESS_PERMIT)) {
			mre.setObligationsRuleIds(
					getObligationRuleIdListFromFiles(dpoInfo.getRequestPath(), dpoInfo.getPolicyPath()));
			mre.setResponse("Permit");
		} else {
			mre.setResponse("Deny");
		}
		return mre;
	}

	private void endAccess(MultiResourceHandlerEvent event) {
		List<DpoInfo> dpoInfoList = componentsWrapper.getDpoInfoStorage().getForSessionIdEh(event.getSessionID());
		if (!dpoInfoList.isEmpty()) {
			endAccessMulti(dpoInfoList);
		}
	}

	public void endAccessSingle(String sessionIdUcs) {
		log.severe("endAccessSingle");
		// ups
	}

	public void endAccessMulti(List<DpoInfo> dpoInfoList) {
		log.severe("endAccessMulti");

//		List<DpoInfo> allowedDpos = dpoInfoList.stream().peek(el -> log.severe("el.getStatus()=" + el.getStatus()))
//				.filter(el -> el.getStatus().equals("STARTACCESS_PERMIT")).collect(Collectors.toList());
		for (int i = 0; i < dpoInfoList.size(); i++) {
			log.log(Level.SEVERE, "endAccessMulti [{0}/{1}]", new Object[] { i + 1, dpoInfoList.size() });
			DpoInfo dpoInfo = dpoInfoList.get(i);
			EndAccessResponseMessage response = sendEndAccess(dpoInfo);
			endAccessSuccess(response, dpoInfo.getSessionIdEh());
		}
		dpoInfoList.stream().forEach(componentsWrapper.getDpoInfoStorage()::delete); // TODO also for try status or deny
	}

	public EndAccessResponseMessage sendEndAccess(DpoInfo dpoInfo) {
		return componentsWrapper.getUcsClient().endAccess(dpoInfo.getSessionIdUcs(), dpoInfo.getSessionIdEh());
	}

//	private void endAccessError(Throwable throwable) {
//		log.info("endAccessError");
//		MultiResourceHandlerEvent event = new MultiResourceHandlerEvent();
//		event.setEventType(MultiResourceHandlerEventTypes.END_ACCESS_RESPONSE);
//		event.setProperty("response", "Deny(Error)");
//		eventHandler.send(event);
//	}

	private void endAccessSuccess(EndAccessResponseMessage message, String sessionId) {
		log.severe("endAccessSuccess");
		List<DpoInfo> list = componentsWrapper.getDpoInfoStorage().getForSessionIdEh(sessionId);

		MultiResourceHandlerEvent event = new MultiResourceHandlerEvent();

		if (list.stream().filter(el -> el.getStatus().equals("REVOKE")).count() > 0) {
			log.severe("inside if in endAccessSuccess");
			log.severe("list.size()=" + list.size());
			log.severe("list of revoked element size: "
					+ list.stream().filter(el -> el.getStatus().equals("REVOKE")).count());
			event.setProperty("revoke", "true");
		}
		event.setEventType(MultiResourceHandlerEventTypes.END_ACCESS_RESPONSE);
		event.setProperty("response", message.getEvaluation().getResponse());
		String sessionIdEh = deleteEntryFromEndAccessSessions(message.getMessageId());

		event.setSessionID(sessionIdEh);
		componentsWrapper.getEventHandler().send(event);
	}

	private String deleteEntryFromEndAccessSessions(String messageId) {
		EndAccessSessions endAccessSessions = componentsWrapper.getEndAccessStorage()
				.getForFields("message_id", messageId).get(0);
		componentsWrapper.getEndAccessStorage().delete(endAccessSessions);
		return endAccessSessions.getSession_id_eh();
	}

//private Optional<Event> buildReevaluationResponseEvent(ReevaluationResponse message) {
//	String response = message.getPDPEvaluation().getResponse();
//	log.info(() -> "Handling Reevaluation : " + response);
//	if (response.equals(DecisionType.DENY.toString())) {
//		getExternalFromInternalSessionId(message.getPDPEvaluation().getSessionId())
//				.ifPresent(this::handleEndAccessMulti);
//		Event event = new Event();
//		event.setEventType(EventTypes.REVOKE_ACCESS);
//		ucs.enrichEvent(event, message.getID());
//		return Optional.of(event);
//	}
//	return Optional.empty();
//}

	private List<String> getObligationRuleIdListFromFiles(String requestPath, String policyPath) {
		try {
			String policy = UcsUtils.readPolicyRaw(policyPath);
			String request = UcsUtils.readRequestRaw(requestPath);
			return getObligationRuleIdList(request, policy);
		} catch (IOException e) {
			log.severe("Error reading policy/request : " + e.getClass().getSimpleName() + ", " + e.getMessage());
		}
		return Collections.emptyList();
	}

	private List<String> getObligationRuleIdList(String request, String policy) {
		policy = UcsUtils.preparePolicyForObligation(policy);
		request = UcsUtils.prepareRequest(request);
		return UcsUtils.getTriggeredObligationRuleIdList(componentsWrapper.getUcs().enrichRequest(request), policy);
	}

	private boolean isTryAccessMultiComplete(String sessionIdEh) {
		return !componentsWrapper.getDpoInfoStorage().getForSessionIdEh(sessionIdEh).isEmpty() && componentsWrapper
				.getDpoInfoStorage().getForSessionIdEhAndStatus(sessionIdEh, DPO_STATUS.TRYACCESS_PENDING).isEmpty();
	}

	private boolean isStartAccessMultiComplete(String sessionIdEh) {
		return !componentsWrapper.getDpoInfoStorage().getForSessionIdEh(sessionIdEh).isEmpty() && componentsWrapper
				.getDpoInfoStorage().getForSessionIdEhAndStatus(sessionIdEh, DPO_STATUS.STARTACCESS_PENDING).isEmpty();
	}

	private int dpoInfoStatusCount(String sessionIdEh, DPO_STATUS status) {
		return componentsWrapper.getDpoInfoStorage().getForSessionIdEhAndStatus(sessionIdEh, status).size();
	}

	private void sendMultiResponseEvent(String messageIdEh, String sessionIdEh) {
		List<DpoInfo> dpoList = componentsWrapper.getDpoInfoStorage().getForSessionIdEh(sessionIdEh);
		String requestId = dpoList.get(0).getRequestId();
		Event responseEvent = createMultiResponseEvent(messageIdEh, sessionIdEh, requestId);
		log.severe("dpoList in sendMultiResponseEvent");
		dpoList.stream().forEach(el -> log.severe(el.getDpoId()));
		dpoList.stream().filter(el -> el.getStatus().equalsIgnoreCase(DPO_STATUS.TRYACCESS_DENY.toString()))
				.forEach(el -> componentsWrapper.getDpoInfoStorage().deleteAllSessionIdEh(sessionIdEh));
		componentsWrapper.getEventHandler().send(responseEvent);
	}

	private Event createMultiResponseEvent(String messageIdEh, String sessionIdEh, String requestId) {
		MultiResourceHandlerEvent event = new MultiResourceHandlerEvent();
		event.setEventType(MultiResourceHandlerEventTypes.TRY_ACCESS_MULTI_RESPONSE); // yes, TRY
		event.setMessageID(messageIdEh);
		event.setSessionID(sessionIdEh);
		event.setRequestID(requestId);
		createMultiResponseFile(sessionIdEh).ifPresent(r -> {
			event.setProperty(UcsUtils.RESPONSE_LINK_KEY, r);
			log.severe(() -> "MultiResponseEvent file saved to : " + r);
		});

		return event;
	}

	private Map<String, String> createContextMap(MultiResourceHandlerEvent event, String requestPath,
			String policyPath) {
		Map<String, String> map = new HashMap<>();
		map.put(UcsUtils.SESSION_ID_KEY, event.getSessionID());
		map.put(UcsUtils.MESSAGE_ID_KEY, event.getMessageID());
		map.put(MultiResourceHandlerEventConstants.REQUEST_ID, event.getRequestID());
		map.put(UcsUtils.REQUEST_PATH_KEY, requestPath);
		map.put(UcsUtils.POLICY_PATH_KEY, policyPath);

		try {
			String policy = UcsUtils.readPolicyRaw(policyPath);
			map.put(UcsUtils.POLICY_KEY, policy);

			String request = UcsUtils.readRequestRaw(requestPath);
			map.put(UcsUtils.REQUEST_KEY, request);

			Optional<String> dpoId = UcsUtils.getResourceIdFromRequest(request);
			dpoId.ifPresent(d -> map.put(UcsUtils.DPO_ID_KEY, d));
			if (!map.containsKey(UcsUtils.DPO_ID_KEY)) {
				log.severe("Error no dpoId found in request");
			}
		} catch (IOException e) {
			log.severe(() -> "Error handling tryAccess element > " + e.getClass().getSimpleName() + " : "
					+ e.getMessage());
		}

		return map;
	}

	private Map<String, String> createContextMap(MultiResourceHandlerEvent event) {
		Map<String, String> map = new HashMap<>();
		map.put(UcsUtils.SESSION_ID_KEY, event.getSessionID());
		map.put(UcsUtils.MESSAGE_ID_KEY, event.getMessageID());
		map.put(MultiResourceHandlerEventConstants.REQUEST_ID, event.getRequestID());
		map.put(UcsUtils.POLICY_KEY, event.getProperty(UcsUtils.POLICY_KEY));
		map.put(UcsUtils.REQUEST_KEY, event.getProperty(UcsUtils.REQUEST_KEY));
		return map;
	}

	private MultiResourceHandlerEvent createEvent(Map<String, String> contextMap, String eventType) {
		MultiResourceHandlerEvent event = new MultiResourceHandlerEvent();
		event.setEventType(eventType);
		event.setSessionID(contextMap.get(UcsUtils.SESSION_ID_KEY));
		event.setMessageID(contextMap.get(UcsUtils.MESSAGE_ID_KEY));
		event.setRequestID(contextMap.get(MultiResourceHandlerEventConstants.REQUEST_ID));
		return event;
	}

}
