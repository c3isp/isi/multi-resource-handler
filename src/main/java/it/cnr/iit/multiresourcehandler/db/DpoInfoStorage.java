package it.cnr.iit.multiresourcehandler.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import it.cnr.iit.common.reject.Reject;

@Component
public class DpoInfoStorage {

	private Logger log = Logger.getLogger(DpoInfoStorage.class.getName());

	private ConnectionSource connection = null;
	private Dao<DpoInfo, Integer> dao = null;

	@Value("${db.connection:jdbc:sqlite::memory:}")
	private String dbConnection;

	@PostConstruct
	public void init() {
		refresh();
	}

	private synchronized void refresh() {
		if (connection == null || !connection.isOpen()) {
			try {
				connection = new JdbcPooledConnectionSource(dbConnection);
				dao = DaoManager.createDao(connection, DpoInfo.class);
				TableUtils.createTableIfNotExists(connection, DpoInfo.class);
				log.info(() -> this.getClass().getSimpleName() + " database connection established.");
			} catch (SQLException e) {
				log.severe(() -> e.getClass().getSimpleName() + " while refreshing connection : " + e.getMessage());
			}
		}
	}

	public boolean createOrUpdate(DpoInfo dpoInfo) {
		try {
			refresh();
			dao.createOrUpdate(dpoInfo);
			return true;
		} catch (SQLException e) {
			log.severe(() -> e.getClass().getSimpleName() + " : " + e.getMessage());
			return false;
		}
	}

	public boolean delete(DpoInfo dpoInfo) {
		try {
			refresh();
			dao.delete(dpoInfo);
			return true;
		} catch (SQLException e) {
			log.severe(() -> e.getClass().getSimpleName() + " : " + e.getMessage());
			return false;
		}
	}

	public void deleteAllSessionIdEh(String sessionIdEh) {
		List<DpoInfo> dpoInfoList = getForSessionIdEh(sessionIdEh);
		if (!dpoInfoList.isEmpty()) {
			log.info(() -> "Deleting " + dpoInfoList.size() + " DpoInfo items for sessionIdEh : " + sessionIdEh);
			dpoInfoList.stream().forEach(this::delete);
		}
	}

	public DpoInfo getForDpoId(String dpoId) {
		return getForField(DpoInfo.DPO_ID_FIELD, dpoId);
	}

	public List<DpoInfo> getForSessionIdEh(String sessionIdEh) {
		return getForFields(DpoInfo.SESSION_ID_EH_FIELD, sessionIdEh);
	}

	public List<DpoInfo> getForSessionIdUcs(String sessionIdUcs) {
		return getForFields(DpoInfo.SESSION_ID_UCS_FIELD, sessionIdUcs);
	}

	public List<DpoInfo> getForSessionIdEhAndStatus(String sessionIdEh, DPO_STATUS status) {
		return getForSessionIdEh(sessionIdEh).stream().filter(c -> c.getStatus().equals(status.toString()))
				.collect(Collectors.toList());
	}

	private DpoInfo getForField(String column, String value) {
		List<DpoInfo> tables = getForFields(column, value);
		if (tables == null || tables.size() > 1) {
			throw new IllegalStateException("Same field used multiple times");
		}
		return tables.stream().findFirst().orElse(null);
	}

	public List<DpoInfo> getForFields(String column, String value) {
		Reject.ifBlank(column);
		Reject.ifBlank(value);
		try {
			refresh();
			QueryBuilder<DpoInfo, Integer> qbAttributes = dao.queryBuilder();
			return qbAttributes.where().eq(column, value).query();
		} catch (Exception e) {
			log.severe(() -> e.getClass().getSimpleName() + " : " + column + " : " + value + " , " + e.getMessage());
		}
		return new ArrayList<>();
	}

}
