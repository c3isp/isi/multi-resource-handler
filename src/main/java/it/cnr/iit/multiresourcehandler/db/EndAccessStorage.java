package it.cnr.iit.multiresourcehandler.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import it.cnr.iit.common.reject.Reject;

@Component
public class EndAccessStorage {
	private Logger log = Logger.getLogger(EndAccessStorage.class.getName());

	private ConnectionSource connection = null;
	private Dao<EndAccessSessions, String> dao = null;

	@Value("${db.connection:jdbc:sqlite::memory:}")
	private String dbConnection;

	@PostConstruct
	public void init() {
		refresh();
	}

	private synchronized void refresh() {
		if (connection == null || !connection.isOpen()) {
			try {
				connection = new JdbcPooledConnectionSource(dbConnection);
				dao = DaoManager.createDao(connection, EndAccessSessions.class);
				TableUtils.createTableIfNotExists(connection, EndAccessSessions.class);
				log.info(() -> this.getClass().getSimpleName() + " database connection established.");
			} catch (SQLException e) {
				log.severe(() -> e.getClass().getSimpleName() + " while refreshing connection : " + e.getMessage());
			}
		}
	}

	public boolean createOrUpdate(EndAccessSessions EndAccessSessions) {
		try {
			refresh();
			dao.createOrUpdate(EndAccessSessions);
			return true;
		} catch (SQLException e) {
			log.severe(() -> e.getClass().getSimpleName() + " : " + e.getMessage());
			return false;
		}
	}

	public boolean delete(EndAccessSessions EndAccessSessions) {
		try {
			refresh();
			dao.delete(EndAccessSessions);
			return true;
		} catch (SQLException e) {
			log.severe(() -> e.getClass().getSimpleName() + " : " + e.getMessage());
			return false;
		}
	}

	public void deleteAllSessionIdEh(String sessionIdEh) {
		List<EndAccessSessions> EndAccessSessionsList = getForSessionIdEh(sessionIdEh);
		if (!EndAccessSessionsList.isEmpty()) {
			log.info(() -> "Deleting " + EndAccessSessionsList.size() + " EndAccessSessions items for sessionIdEh : "
					+ sessionIdEh);
			EndAccessSessionsList.stream().forEach(this::delete);
		}
	}

	public EndAccessSessions getForMessageId(String messageId) {
		return getForField(EndAccessSessions.MESSAGE_ID_FIELD, messageId);
	}

	public List<EndAccessSessions> getForSessionIdEh(String sessionIdEh) {
		return getForFields(EndAccessSessions.SESSION_ID_EH_FIELD, sessionIdEh);
	}

	private EndAccessSessions getForField(String column, String value) {
		List<EndAccessSessions> tables = getForFields(column, value);
		if (tables == null || tables.size() > 1) {
			throw new IllegalStateException("Same field used multiple times");
		}
		return tables.stream().findFirst().orElse(null);
	}

	public List<EndAccessSessions> getForFields(String column, String value) {
		Reject.ifBlank(column);
		Reject.ifBlank(value);
		try {
			refresh();
			QueryBuilder<EndAccessSessions, String> qbAttributes = dao.queryBuilder();
			return qbAttributes.where().eq(column, value).query();
		} catch (Exception e) {
			log.severe(() -> e.getClass().getSimpleName() + " : " + column + " : " + value + " , " + e.getMessage());
		}
		return new ArrayList<>();
	}
}
