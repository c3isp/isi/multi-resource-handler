package it.cnr.iit.multiresourcehandler.db;

import com.google.gson.Gson;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "endAccess_sessions")
public class EndAccessSessions {
	private static final Gson gson = new Gson();
	public static final String SESSION_ID_UCS_FIELD = "session_id_ucs";
	public static final String SESSION_ID_EH_FIELD = "session_id_eh";
	public static final String MESSAGE_ID_FIELD = "message_id";

	@DatabaseField(id = true, columnName = MESSAGE_ID_FIELD)
	private String message_id;

	@DatabaseField(columnName = SESSION_ID_UCS_FIELD)
	private String session_id_ucs;

	@DatabaseField(columnName = SESSION_ID_EH_FIELD)
	private String session_id_eh;

	public String getMessage_id() {
		return message_id;
	}

	public void setMessage_id(String message_id) {
		this.message_id = message_id;
	}

	public String getSession_id_ucs() {
		return session_id_ucs;
	}

	public void setSession_id_ucs(String session_id_ucs) {
		this.session_id_ucs = session_id_ucs;
	}

	public String getSession_id_eh() {
		return session_id_eh;
	}

	public void setSession_id_eh(String session_id_eh) {
		this.session_id_eh = session_id_eh;
	}

}
