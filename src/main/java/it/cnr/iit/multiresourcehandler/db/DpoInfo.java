package it.cnr.iit.multiresourcehandler.db;

import com.google.gson.Gson;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import it.cnr.iit.clients.dpos.DpoMetadata;

@DatabaseTable(tableName = "dpo_info_table")
public class DpoInfo {
	private static final Gson gson = new Gson();
	public static final String ID_FIELD = "id";
	public static final String DPO_ID_FIELD = "dpo_id";
	public static final String DPO_METADATA_FIELD = "dpo_metadata";
	public static final String STATUS_FIELD = "status";
	public static final String POLICY_PATH_FIELD = "policy_path";
	public static final String REQUEST_PATH_FIELD = "request_path";
	public static final String SESSION_ID_UCS_FIELD = "session_id_ucs";
	public static final String SESSION_ID_EH_FIELD = "session_id_eh";
	public static final String MESSAGE_ID_EH_FIELD = "message_id_eh";
	public static final String REQUEST_ID_FIELD = "request_id";

	@DatabaseField(generatedId = true, columnName = ID_FIELD)
	private Integer id;

	@DatabaseField(columnName = DPO_ID_FIELD)
	private String dpoId;

	// columnDefinition = "TEXT DEFAULT NULL")
	@DatabaseField(columnName = DPO_METADATA_FIELD, dataType = DataType.SERIALIZABLE)
	private DpoMetadata dpoMetadata;

	@DatabaseField(columnName = STATUS_FIELD)
	private String status;

	@DatabaseField(columnName = POLICY_PATH_FIELD)
	private String policyPath;

	@DatabaseField(columnName = REQUEST_PATH_FIELD)
	private String requestPath;

	@DatabaseField(columnName = SESSION_ID_UCS_FIELD)
	private String sessionIdUcs;

	@DatabaseField(columnName = SESSION_ID_EH_FIELD)
	private String sessionIdEh;

	@DatabaseField(columnName = MESSAGE_ID_EH_FIELD)
	private String messageIdEh;

	@DatabaseField(columnName = REQUEST_ID_FIELD)
	private String requestId;

	public Integer getId() {
		return id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setStatus(DPO_STATUS status) {
		this.status = status.toString();
	}

	public boolean isStatus(DPO_STATUS status) {
		return getStatus().equals(status.toString());
	}

	public String getDpoId() {
		return dpoId;
	}

	public void setDpoId(String dpoId) {
		this.dpoId = dpoId;
	}

	public DpoMetadata getDpoMetadata() {
		return dpoMetadata;
		// return gson.fromJson(dpoMetadata, DpoMetadata.class);
	}

	public void setDpoMetadata(DpoMetadata dpoMetadata) {
		this.dpoMetadata = dpoMetadata;
		// this.dpoMetadata = gson.toJson(dpoMetadata);
	}

	public String getPolicyPath() {
		return policyPath;
	}

	public void setPolicyPath(String policyPath) {
		this.policyPath = policyPath;
	}

	public String getRequestPath() {
		return requestPath;
	}

	public void setRequestPath(String requestPath) {
		this.requestPath = requestPath;
	}

	public String getSessionIdUcs() {
		return sessionIdUcs;
	}

	public void setSessionIdUcs(String sessionIdUcs) {
		this.sessionIdUcs = sessionIdUcs;
	}

	public String getSessionIdEh() {
		return sessionIdEh;
	}

	public void setSessionIdEh(String sessionIdEh) {
		this.sessionIdEh = sessionIdEh;
	}

	public String getMessageIdEh() {
		return messageIdEh;
	}

	public void setMessageIdEh(String messageIdEh) {
		this.messageIdEh = messageIdEh;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

}
