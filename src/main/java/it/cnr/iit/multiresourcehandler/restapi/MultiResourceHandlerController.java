/**
 * Copyright 2017 Hewlett Packard Enterprise Development Company, L.P.
 */
package it.cnr.iit.multiresourcehandler.restapi;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.cnr.iit.common.eventhandler.events.ResponseEvent;
import it.cnr.iit.common.eventhandler.events.multiresourcehandler.MultiResourceHandlerEvent;
import it.cnr.iit.multiresourcehandler.MultiResourceHandler;

@ApiModel(value = "MRH", description = "Multi Resource Handler API")
@RestController
@RequestMapping("/")
public class MultiResourceHandlerController {

	private static final Logger log = Logger.getLogger(MultiResourceHandlerController.class.getName());

	@Autowired
	private MultiResourceHandler mrh;

	@ApiOperation(httpMethod = "POST", value = "Receives an event from the EventHandler")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid message received"),
			@ApiResponse(code = 200, message = "OK") })
	@PostMapping(value = "eventArrived", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEvent eventArrived(@RequestBody() MultiResourceHandlerEvent event) {
		try {
			log.severe(() -> "Event of type " + event.getEventType() + " received: " + event);
			log.severe("event arrived to mrh: " + new ObjectMapper().writeValueAsString(event));
			mrh.handleEvent(event);
			return new ResponseEvent(true);
		} catch (Exception e) {
			log.info(() -> "EventArrived error : " + e.getClass().getSimpleName() + " : " + e.getMessage());
			e.printStackTrace();
			return new ResponseEvent(false, e.getMessage());
		}
	}

}
