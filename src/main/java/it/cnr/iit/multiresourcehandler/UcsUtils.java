package it.cnr.iit.multiresourcehandler;

import java.io.IOException;
import java.io.StringReader;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import it.cnr.iit.common.attributes.AttributeIds;
import it.cnr.iit.common.types.DataManipulationObject;
import it.cnr.iit.common.utility.XMLUtility;
import it.cnr.iit.obligationengine.obligations.ObligationEngine;
import it.cnr.iit.obligations.PolicyDecisionPoint;
import it.cnr.iit.ucs.constants.STATUS;
import it.cnr.iit.ucs.pdp.PDPEvaluation;
import it.cnr.iit.xacml.Category;
import oasis.names.tc.xacml.core.schema.wd_17.PolicyType;
import oasis.names.tc.xacml.core.schema.wd_17.RequestType;
import oasis.names.tc.xacml.core.schema.wd_17.RuleType;

public class UcsUtils {

	private static final Logger log = Logger.getLogger(UcsUtils.class.getName());

	public static final String POLICY_KEY = "xacmlPolicy";
	public static final String REQUEST_KEY = "xacmlRequest";
	public static final String POLICY_PATH_KEY = "policyPath";
	public static final String REQUEST_PATH_KEY = "requestPath";
	public static final String DPO_ID_KEY = "dpoId";
	public static final String MESSAGE_ID_KEY = "messageId";
	public static final String SESSION_ID_KEY = "sessionId";
	public static final String RESPONSE_LINK_KEY = "response_link";

	public static final String RESPONSE = "response";
	public static final String LONG_RESPONSE = "longResponse";
	public static final String OBLIGATIONS = "obligations";
	public static final String OBLIGATIONS_RESPONSE = "obligationsResponse";

	public static final String SESSION_ID_DEFAULT_VALUE = "000";

	private UcsUtils() {

	}

	public static List<String> getTriggeredObligationRuleIdList(String request, String policy) {
		List<String> obligationRuleIdList = new ArrayList<>();
		log.info("Evaluating with custom pdp");
		try {
			PDPEvaluation response = new PolicyDecisionPoint().evaluate(request, policy, STATUS.TRY);

			log.severe("response = " + response.getResponse());
			if (response.getObligations() != null) {
				obligationRuleIdList = response.getObligations();
			}

		} catch (Exception e) {
			log.severe("Error evaluating policy with custom pdp : " + e.getClass().getSimpleName() + ", "
					+ e.getMessage());
			e.printStackTrace();
		}

		return obligationRuleIdList;
	}

	public static List<DataManipulationObject> performObligation(String policy, String triggeredObligations) {
		String[] obls = unmarshalTriggeredObligations(triggeredObligations);
		Document doc = convertStringToXMLDocument(policy);
		List<Node> triggeredList = new ArrayList<Node>();
		try {
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			XPathExpression expr = xpath.compile("//ObligationExpression");
			NodeList extractedElements = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			for (int i = 0; i < extractedElements.getLength(); i++) {
				Node element = extractedElements.item(i);
				Node xacmlRuleNode = element.getParentNode().getParentNode();
				for (int j = 0; j < obls.length; j++) {
					if (xacmlRuleNode.getAttributes().getNamedItem("RuleId").getNodeValue().compareTo(obls[j]) == 0) {
						log.severe(
								"target found: " + xacmlRuleNode.getAttributes().getNamedItem("RuleId").getNodeValue());
						String val = element.getAttributes().getNamedItem("ObligationId").getNodeValue();
						String val_decoded = URLDecoder.decode(val, StandardCharsets.UTF_8.toString()).toString();
						String actionID = val.split("%")[0];
						String attribute = val_decoded.split("\\{")[1].split("\\}")[0];
						log.severe("detected actionID: " + actionID);
						log.severe("detected attribute: " + attribute);
						triggeredList.add(element);
					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		List<DataManipulationObject> dmos = new ArrayList<DataManipulationObject>();
		for (Node n : triggeredList) {
			DataManipulationObject dmo = ObligationEngine.processObligation(n);
			if (dmo != null) {
				dmos.add(dmo);
			}
		}
		return dmos;

	}

	public static String[] unmarshalTriggeredObligations(String obligations) {
		obligations = obligations.substring(1, obligations.length() - 1);
		String[] res = obligations.split(",");
		for (int i = 0; i < res.length; i++) {
			res[i] = res[i].substring(1, res[i].length() - 1);
		}

		return res;
	}

	public static Document convertStringToXMLDocument(String xmlString) {
		// Parser that produces DOM object trees from XML content
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		// API to obtain DOM Document instance
		DocumentBuilder builder = null;
		try {
			// Create DocumentBuilder with default configuration
			builder = factory.newDocumentBuilder();

			// Parse the content to Document object
			Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
			return doc;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static List<RuleType> getRuleTypeList(PolicyType policyType) {
		return getObjectsOfTypeToList(policyType, RuleType.class);
	}

	@SuppressWarnings("unchecked")
	private static <T> List<T> getObjectsOfTypeToList(PolicyType policyType, Class<T> clazz) {
		List<Object> list = policyType.getCombinerParametersOrRuleCombinerParametersOrVariableDefinition();
		return (List<T>) list.stream().filter(clazz::isInstance).collect(Collectors.toList());
	}

	public static Optional<String> getResourceIdFromRequest(String request) {
		return getAttributeFromRequest(request, Category.RESOURCE.toString(), AttributeIds.RESOURCE_ID);
	}

	public static Optional<String> getDSAIdFromRequest(String request) {
		return getAttributeFromRequest(request, Category.RESOURCE.toString(), AttributeIds.DSA_ID);
	}

	public static Optional<String> getActionIdFromRequest(String request) {
		return getAttributeFromRequest(request, Category.ACTION.toString(), AttributeIds.ACTION_ID);
	}

	private static Optional<String> getAttributeFromRequest(String request, String category, String attributeId) {
		Optional<RequestType> requestType = XMLUtility.objFromXMLOptional(RequestType.class, request);
		if (requestType.isPresent()) {
			String value = requestType.get().getAttribute(category, attributeId);
			if (value != null) {
				return Optional.of(value);
			}
		}
		log.warning(() -> "Attribute " + attributeId + " not found.");
		return Optional.empty();
	}

	public static String preparePolicy(String policy) {
		policy = policy.replaceAll("dsa-status:\\w+", "dsa-status").replaceAll("dsa-version:\\w+", "dsa-version")
				.replaceAll("subject:subject-id:\\w+", "subject:subject-id")
				.replaceAll("subject:subject-organisation:\\w+", "subject:subject-organisation")
				.replaceAll("subject:subject-role:\\w+", "subject:subject-role")
				.replaceAll("subject:subject-ismemberof:\\w+", "subject:subject-ismemberof")
				.replaceAll("subject:subject-country:\\w+", "subject:subject-country")
				.replaceAll("subject:access-purpose:\\w+", "subject:access-purpose")
				.replaceAll("action:action-id:\\w+", "action:action-id")
				.replaceAll("resource:resource-type:\\w+", "resource:resource-type")
				.replaceAll("resource:resource-owner:\\w+", "resource:resource-owner")
				.replaceAll("resource:resource-data-end-time:\\w+", "resource:data-end-time")
				.replaceAll("resource:resource-data-start-time:\\w+", "resource:data-start-time")
				.replaceAll("resource:resource-data-end-date:\\w+", "resource:data-end-date")
				.replaceAll("resource:resource-data-start-date:\\w+", "resource:data-start-date")
				.replaceAll("resource:dsa-id:\\w+", "resource:dsa-id")
				.replaceAll("environment:current-date:\\w+", "environment:current-date")
				.replaceAll("environment:current-time:\\w+", "environment:current-time");
		return patternToLower(policy, "Invoke\\w+");
	}

	public static String preparePolicyForAuthorization(String policy) {
		String filteredPolicy = XacmlUtils.filterRuleIdPrefix(policy, "OBLIGATION").orElse(policy);
		return preparePolicy(filteredPolicy).replace("<Match", "<xacml:Match").replace("</Match", "</xacml:Match");
	}

	public static String preparePolicyForObligation(String policy) {
		String filteredPolicy = XacmlUtils.filterRuleIdPrefix(policy, "AUTHORIZATION", "PROHIBITION").orElse(policy);
		final String COMBINATION_ALGORITHM_ID_PREFIX = "urn:oasis:names:tc:xacml:1.0:rule-combining-algorithm:";
		final String COMBINATION_ALGORITHM_ID_REGEX = COMBINATION_ALGORITHM_ID_PREFIX + "[a-z\\-]+";
		final String COMBINATION_ALGORITHM_ID_CUSTOM = COMBINATION_ALGORITHM_ID_PREFIX
				+ "first-applicable-with-obligations";
		return preparePolicy(filteredPolicy).replaceAll(COMBINATION_ALGORITHM_ID_REGEX,
				COMBINATION_ALGORITHM_ID_CUSTOM);
	}

	public static String preparePolicyForCohabitation(String policy) {
		return preparePolicy(policy);
	}

	public static String prepareRequest(String request) {
//		String translatedRequest = XacmlUtils.translateDpoMetadata(request).orElse(request);
//		return patternToLower(translatedRequest, "Invoke\\w+");
		return patternToLower(request, "Invoke\\w+");

	}

	public static String readPolicyForAuthorization(String policyPath) throws IOException {
		return preparePolicyForAuthorization(readPolicyRaw(policyPath));
	}

	public static String readPolicyForObligation(String policyPath) throws IOException {
		return preparePolicyForObligation(readPolicyRaw(policyPath));
	}

	public static String readRequest(String requestPath) throws IOException {
		return prepareRequest(readRequestRaw(requestPath));
	}

	public static String readPolicyRaw(String policyPath) throws IOException {
		return new String(Files.readAllBytes(Paths.get(policyPath)));
	}

	public static String readRequestRaw(String requestPath) throws IOException {
		return new String(Files.readAllBytes(Paths.get(requestPath)));
	}

	public static String patternToLower(String data, String patternStr) {
		Pattern pattern = Pattern.compile(patternStr);
		Matcher m = pattern.matcher(data);
		while (m.find()) {
			String s = m.group(0);
			data = data.replaceAll(s, s.toLowerCase());
		}
		return data;
	}

}
