package it.cnr.iit.multiresourcehandler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.function.UnaryOperator;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.traversal.DocumentTraversal;
import org.w3c.dom.traversal.NodeFilter;
import org.w3c.dom.traversal.NodeIterator;
import org.wso2.balana.utils.Constants.PolicyConstants.DataType;
import org.xml.sax.SAXException;

import it.cnr.iit.clients.dpos.DpoMetadata;
import it.cnr.iit.clients.dpos.DposClient;
import it.cnr.iit.utils.TimeUtils;

public class XacmlUtils {

	private static final Logger log = Logger.getLogger(XacmlUtils.class.getName());

	public static final String resourceIdAttributeId = "urn:oasis:names:tc:xacml:1.0:resource:resource-id";
	public static final String dpoMetadataAttributeId = "urn:oasis:names:tc:xacml:3.0:resource:dpo_metadata";
	public static final String resStartDateAttributeId = "urn:oasis:names:tc:xacml:1.0:resource:data-start-date";
	public static final String resEndDateAttributeId = "urn:oasis:names:tc:xacml:1.0:resource:data-end-date";
	public static final String resStartTimeAttributeId = "urn:oasis:names:tc:xacml:1.0:resource:data-start-time";
	public static final String resEndTimeAttributeId = "urn:oasis:names:tc:xacml:1.0:resource:data-end-time";
	public static final String resTypeAttributeId = "urn:oasis:names:tc:xacml:3.0:resource:resource-type";
	public static final String resOwnerAttributeId = "urn:oasis:names:tc:xacml:3.0:resource:resource-owner";

	public static final String ATTRIBUTE_ID = "AttributeId";

	private static DposClient dposUtils;

	private XacmlUtils() {
		// avoid allocation
	}

	public static Optional<String> translateDpoMetadata(String request) {
		String resourceId = UcsUtils.getResourceIdFromRequest(request).orElse("<unavailable>");
		String actionId = UcsUtils.getActionIdFromRequest(request).orElse("<unavailable>");

		log.info(() -> "TranslateDpoMetadata for dpoId : " + resourceId);
		try {
			Document doc = load(request);
			DocumentTraversal traversal = (DocumentTraversal) doc;
			Node root = doc.getDocumentElement();
			NodeIterator iterator = traversal.createNodeIterator(root, NodeFilter.SHOW_ELEMENT, null, true);

			Node resourceIdNode = null;
			Node dpoMetadataNode = null;

			for (Node n = iterator.nextNode(); n != null; n = iterator.nextNode()) {
				if (isNodeType(n, "attribute")) {
					Element e = (Element) n;
					if (nodeAttributeEquals(n, ATTRIBUTE_ID, dpoMetadataAttributeId)) {
						Node parentNode = e.getParentNode();
						parentNode.removeChild(e);
						dpoMetadataNode = n;
					} else if (nodeAttributeEquals(n, ATTRIBUTE_ID, resourceIdAttributeId)) {
						resourceIdNode = n;
					}
				}
			}

			DpoMetadata dpoMetadata = null;
			boolean isCreate = actionId.equalsIgnoreCase("create");
			if (isCreate && dpoMetadataNode != null) {
				Node childNode = findChild(dpoMetadataNode);
				if (childNode != null) {
					dpoMetadata = DpoMetadata.fromJson(childNode.getTextContent());
				}
			}

			if (!isCreate && resourceIdNode != null) {
				dpoMetadata = dposUtils.getDpoMetadata(resourceId).orElse(null);
			}

			if (dpoMetadata != null) {
				processDpoMetadata(resourceIdNode, dpoMetadata);
			} else {
				log.info("TranslateDpoMetadata : no valid dpo-metadata found");
			}
			return Optional.of(nodeToXml(root));
		} catch (SAXException | IOException | ParserConfigurationException e) {
			log.severe(() -> "Error parsing policy : " + e.getClass().getSimpleName() + ", " + e.getMessage());
		}
		log.info("TranslateDpoMetadata : no dpo-metadata found");
		return Optional.of(request);
	}

	public static void processDpoMetadata(Node node, DpoMetadata dpoMetadata) {
		Element e = (Element) node;
		Node parentNode = e.getParentNode();
		propertyToNewNode(dpoMetadata.getStartTime(), node, resStartDateAttributeId, DataType.DATE,
				TimeUtils::parseDate).ifPresent(parentNode::appendChild);
		propertyToNewNode(dpoMetadata.getEndTime(), node, resEndDateAttributeId, DataType.DATE, TimeUtils::parseDate)
				.ifPresent(parentNode::appendChild);
		propertyToNewNode(dpoMetadata.getStartTime(), node, resStartTimeAttributeId, DataType.TIME,
				TimeUtils::parseTime).ifPresent(parentNode::appendChild);
		propertyToNewNode(dpoMetadata.getEndTime(), node, resEndTimeAttributeId, DataType.TIME, TimeUtils::parseTime)
				.ifPresent(parentNode::appendChild);
		propertyToNewNode(dpoMetadata.getEventType(), node, resTypeAttributeId, DataType.STRING, s -> s)
				.ifPresent(parentNode::appendChild);
		propertyToNewNode(dpoMetadata.getOrganization(), node, resOwnerAttributeId, DataType.STRING, s -> s)
				.ifPresent(parentNode::appendChild);
	}

	private static Optional<Node> propertyToNewNode(String property, Node node, String attributeId, String dataType,
			UnaryOperator<String> f) {
		String value = f.apply(property);
		Node newNode = cloneNodeAndUpdateFields(node, attributeId, dataType, value);
		return Optional.of(newNode);
	}

	public static Optional<String> filterRuleIdPrefix(String policy, String... prefixes) {
		try {
			Document doc = load(policy);

			DocumentTraversal traversal = (DocumentTraversal) doc;
			Node root = doc.getDocumentElement();
			NodeIterator iterator = traversal.createNodeIterator(root, NodeFilter.SHOW_ELEMENT, null, true);
			for (Node n = iterator.nextNode(); n != null; n = iterator.nextNode()) {
				Element e = (Element) n;
				String tagName = e.getTagName().toLowerCase();
				if (tagName.endsWith("rule")) {
					String ruleId = e.getAttribute("RuleId");
					for (String prefix : prefixes) {
						if (ruleId.startsWith(prefix)) {
							log.info(() -> "Removing " + tagName + "{" + ruleId + "} from policy");
							root.removeChild(e);
						}
					}
				}
			}
			return Optional.of(nodeToXml(root));
		} catch (SAXException | IOException | ParserConfigurationException e) {
			log.severe(() -> "Error parsing policy : " + e.getClass().getSimpleName() + ", " + e.getMessage());
		}
		return Optional.empty();
	}

	private static Node cloneNodeAndUpdateFields(Node node, String attributeId, String dataType, String value) {
		Node newNode = node.cloneNode(true);
		Node childNode = findChild(newNode);
		if (childNode != null) {
			childNode.setTextContent(value);
			setNodeAttribute(childNode, "DataType", dataType);
		}
		setNodeAttribute(newNode, ATTRIBUTE_ID, attributeId);
		return newNode;
	}

	private static void setNodeAttribute(Node node, String attribute, String value) {
		NamedNodeMap attr = node.getAttributes();
		Node nodeAttr = attr.getNamedItem(attribute);
		nodeAttr.setTextContent(value);
	}

	private static Node findChild(Node node) {
		for (int idx = 0; idx < node.getChildNodes().getLength(); idx++) {
			Node childNode = node.getChildNodes().item(idx);
			if (childNode != null && !childNode.getTextContent().trim().isEmpty()) {
				return childNode;
			}
		}
		return null;
	}

	private static boolean isNodeType(Node node, String type) {
		Element e = (Element) node;
		return e.getTagName().toLowerCase().endsWith(type);
	}

	private static boolean nodeAttributeEquals(Node node, String attribute, String value) {
		Element e = (Element) node;
		return e.getAttribute(attribute).equals(value);
	}

	private static String nodeToXml(Node node) {
		StringWriter sw = new StringWriter();
		try {
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer t = transformerFactory.newTransformer();
			t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			t.setOutputProperty(OutputKeys.INDENT, "no");
			t.transform(new DOMSource(node), new StreamResult(sw));
		} catch (TransformerException e) {
			log.severe(() -> "Error transforming xml : " + e.getClass().getSimpleName() + ", " + e.getMessage());
		}
		return sw.toString();
	}

	private static Document load(String xml) throws SAXException, IOException, ParserConfigurationException {
		return load(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8)));
	}

	private static Document load(InputStream is) throws SAXException, IOException, ParserConfigurationException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setExpandEntityReferences(false);
		factory.setNamespaceAware(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		return builder.parse(is);
	}

	public static void setDposUtils(DposClient dposUtils) {
		XacmlUtils.dposUtils = dposUtils;
	}

}
