package it.cnr.iit.clients.dpos;

import java.util.Optional;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class DposClient {

	private static final Logger log = Logger.getLogger(DposClient.class.getName());

	private static final String DPO_ID_TAG = "[%DPO_ID%]";
	private static final String SEARCH_STRING = "{\"combiningRule\":\"or\",\"criteria\":[{\"attribute\":\"id\",\"operator\":\"eq\",\"value\":\""
			+ DPO_ID_TAG + "\"}]}";

	@Autowired
	private DposProperties properties;

	@Autowired
	private RestTemplate restTemplate;

	public Optional<DpoMetadata> getDpoMetadata(String dpoId) {
		String url = properties.getSearchDpoAPI(true);
		log.info(() -> "Fetching dpo metadata for " + dpoId + " from dpos");
		String searchString = SEARCH_STRING.replace(DPO_ID_TAG, dpoId);
		try {
			String res = restTemplate.postForEntity(url + "/", searchString, String.class).getBody().replace("[\"", "")
					.replace("\"]", "").replace("\\", "").trim();
			log.info(() -> "DpoMetadata : " + res);
			if (res != null && !res.isEmpty()) {
				return Optional.of(DpoMetadata.fromJson(res));
			}
		} catch (Exception e) {
			log.severe("Error fetching dpo metadata for " + dpoId + " from dpos -> " + e.getClass() + " : "
					+ e.getMessage());
		}
		return Optional.empty();
	}

}
