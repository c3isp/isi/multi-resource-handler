package it.cnr.iit.clients.dpos;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.util.UriComponentsBuilder;

@Configuration
public class DposProperties {

	@Value("${api.dpos.uri:https://isic3isp.iit.cnr.it:8443/dpos-api/}")
	@NotNull
	private String uri;

	@Value("${api.dpos.searchdpo:/v1/searchDPO}")
	@NotNull
	private String searchDpo;

	public String getUri() {
		return uri;
	}

	public String getSearchDpo() {
		return searchDpo;
	}

	public String getSearchDpoAPI(boolean longResult) {
		return UriComponentsBuilder.fromUriString(getUri()).path(getSearchDpo())
				.pathSegment(String.valueOf(longResult)).build().toUri().toString();
	}

}
