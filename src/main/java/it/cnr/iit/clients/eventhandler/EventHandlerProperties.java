package it.cnr.iit.clients.eventhandler;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.util.UriComponentsBuilder;

@Configuration
public class EventHandlerProperties {

	@Value("${api.eventhandler.uri}")
	@NotNull
	private String uri;

	@Value("${api.eventhandler.subscribers}")
	@NotNull
	private String subscribers;

	@Value("${api.eventhandler.subscribe}")
	@NotNull
	private String subscribe;

	@Value("${api.eventhandler.notifyevent}")
	@NotNull
	private String notifyEvent;

	@Value("${api.multiresourcehandler.uri}")
	@NotNull
	private String mrhUri;

	@Value("${api.multiresourcehandler.eventarrived}")
	@NotNull
	private String eventArrived;

	public String getSubscribersAPI() {
		return getApiUri(getUri(), getSubscribers());
	}

	public String getSubscribeAPI() {
		return getApiUri(getUri(), getSubscribe());
	}

	public String getNotifyEventAPI() {
		return getApiUri(getUri(), getNotifyEvent());
	}

	public String getEventArrivedAPI() {
		return getApiUri(getMrhUri(), getEventArrived());
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getSubscribers() {
		return subscribers;
	}

	public void setSubscribers(String subscribers) {
		this.subscribers = subscribers;
	}

	public String getSubscribe() {
		return subscribe;
	}

	public void setSubscribe(String subscribe) {
		this.subscribe = subscribe;
	}

	public String getNotifyEvent() {
		return notifyEvent;
	}

	public void setNotifyEvent(String notifyEvent) {
		this.notifyEvent = notifyEvent;
	}

	public String getMrhUri() {
		return mrhUri;
	}

	public void setMrhUri(String mrhUri) {
		this.mrhUri = mrhUri;
	}

	public String getEventArrived() {
		return eventArrived;
	}

	public void setEventArrived(String eventArrived) {
		this.eventArrived = eventArrived;
	}

	private String getApiUri(String url, String path) {
		return UriComponentsBuilder.fromUriString(url).path(path).build().toUri().toString();
	}

}
