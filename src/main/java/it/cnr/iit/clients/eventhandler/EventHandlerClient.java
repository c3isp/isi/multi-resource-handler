package it.cnr.iit.clients.eventhandler;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.cnr.iit.common.eventhandler.events.Event;
import it.cnr.iit.common.utility.RESTUtils;

@Component
public class EventHandlerClient {

	private static final Logger log = Logger.getLogger(EventHandlerClient.class.getName());

	@Autowired
	private EventHandlerProperties properties;

	public void send(Event event) {
		log.info(() -> "Sending event to EventHandler : " + " " + event.toString());
		RESTUtils.post(properties.getNotifyEventAPI(), event);
	}

}
