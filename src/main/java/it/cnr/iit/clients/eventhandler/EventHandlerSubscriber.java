package it.cnr.iit.clients.eventhandler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import it.cnr.iit.common.eventhandler.AbstractEventHandlerSubscriber;
import it.cnr.iit.common.eventhandler.events.multiresourcehandler.MultiResourceHandlerEventTypes;

@Configuration
public class EventHandlerSubscriber extends AbstractEventHandlerSubscriber {

	@Autowired
	private EventHandlerProperties properties;

	public EventHandlerSubscriber() {
		super(new String[] { MultiResourceHandlerEventTypes.TRY_ACCESS, MultiResourceHandlerEventTypes.TRY_ACCESS_MULTI,
				MultiResourceHandlerEventTypes.START_ACCESS, MultiResourceHandlerEventTypes.END_ACCESS });
		setScheduleRate(10, 120);
		start();
	}

	@Override
	public String getEventHandlerSubscribeAPI() {
		return properties.getSubscribeAPI();
	}

	@Override
	public String getEventHandlerSubscribersAPI() {
		return properties.getSubscribersAPI();
	}

	@Override
	public String getSelfEventArrivedAPI() {
		return properties.getEventArrivedAPI();
	}

}
