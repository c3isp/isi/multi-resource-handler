package it.cnr.iit.clients.dsamapper;

import java.util.Optional;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class DsaMapperClient {

	private static final Logger log = Logger.getLogger(DsaMapperClient.class.getName());

	private static String DSA_TYPE = "DO-P";

	@Autowired
	private DsaMapperProperties properties;

	@Autowired
	private RestTemplate restTemplate;

	public Optional<String> getOtherDataPolicy(String dsaId) {
		String url = properties.getBuildUpolAPI(dsaId, DSA_TYPE);
		log.info(() -> "Fetching other data policy on dsaMapper : " + url);
		try {
			return Optional.of(restTemplate.postForEntity(url, null, String.class).getBody());
		} catch (Exception e) {
			log.severe("Error fetching other data policy on dsaMapper -> " + e.getClass() + " : " + e.getMessage());
		}
		return Optional.empty();
	}
}
