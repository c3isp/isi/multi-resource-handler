package it.cnr.iit.clients.dsamapper;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.util.UriComponentsBuilder;

@Configuration
public class DsaMapperProperties {

	@Value("${api.dsa-mapper.uri}")
	@NotNull
	private String uri;

	@Value("${api.dsa-mapper.build-upol}")
	@NotNull
	private String buildUpol;

	public String getUri() {
		return uri;
	}

	public String getBuildUpol() {
		return buildUpol;
	}

	public String getBuildUpolAPI(String dsaId, String dsaType) {
		return UriComponentsBuilder.fromUriString(getUri()).path(getBuildUpol()).pathSegment(dsaId + "," + dsaType)
				.build().toUri().toString();
	}

}
