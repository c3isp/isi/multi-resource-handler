package it.cnr.iit.clients.ucs;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import it.cnr.iit.multiresourcehandler.ComponentsWrapper;
import it.cnr.iit.multiresourcehandler.db.DPO_STATUS;
import it.cnr.iit.multiresourcehandler.db.DpoInfo;
import it.cnr.iit.ucs.message.Message;
import it.cnr.iit.ucs.message.endaccess.EndAccessResponseMessage;
import it.cnr.iit.ucs.message.reevaluation.ReevaluationResponseMessage;
import it.cnr.iit.ucs.message.startaccess.StartAccessResponseMessage;
import it.cnr.iit.ucs.message.tryaccess.TryAccessResponseMessage;
import it.cnr.iit.ucs.pep.PEPInterface;
import it.cnr.iit.ucs.properties.components.PepProperties;
import it.cnr.iit.utility.RESTUtils;
import it.cnr.iit.utility.errorhandling.Reject;

public class PEP implements PEPInterface {

	private PepProperties properties;
	private URI uri;

	private ComponentsWrapper componentsWrapper;

	private static final Logger log = Logger.getLogger(PEP.class.getName());

	public PEP(PepProperties properties, ComponentsWrapper componentsWrapper) {

		this.componentsWrapper = componentsWrapper;

		log.severe("inside PEP constuctor");

		if (componentsWrapper == null) {
			log.severe("componentsWrapper is null");
		}

		Reject.ifNull(properties);
		this.properties = properties;
		Optional<URI> opturi = RESTUtils.parseUri(properties.getUri());
		Reject.ifAbsent(opturi, "error parsing uri");
		this.uri = opturi.get(); // NOSONAR
	}

	@Override
	// TODO return actual response.
	public Message onGoingEvaluation(ReevaluationResponseMessage message) {
		if (message.getEvaluation().getResult().equalsIgnoreCase("deny")) {
			List<DpoInfo> dpoInfoList = componentsWrapper.getDpoInfoStorage()
					.getForSessionIdUcs(message.getSessionId());
			dpoInfoList.stream().forEach(el -> {
				el.setStatus(DPO_STATUS.REVOKE);
				componentsWrapper.getDpoInfoStorage().createOrUpdate(el);
			});
			componentsWrapper.getMultiResourceHandler().endAccessMulti(dpoInfoList);
		}
		return null;
	}

	@Override
	// TODO return actual response.
	public String receiveResponse(Message message) {
		Optional<String> api = getApiForMessage(message);
		try {
			RESTUtils.asyncPost(uri.toString(), api.get(), message); // NOSONAR
		} catch (Exception e) {
			log.severe("Error posting message : " + api);
			return "KO";
		}
		return "OK";
	}

	private Optional<String> getApiForMessage(Message message) {
		if (message instanceof TryAccessResponseMessage) {
			return Optional.of(properties.getApiTryAccessResponse());
		} else if (message instanceof StartAccessResponseMessage) {
			return Optional.of(properties.getApiStartAccessResponse());
		} else if (message instanceof EndAccessResponseMessage) {
			return Optional.of(properties.getApiEndAccessResponse());
		}
		return Optional.empty();
	}

	public PepProperties getProperties() {
		return properties;
	}

	public void setProperties(PepProperties properties) {
		this.properties = properties;
	}

}
