package it.cnr.iit.clients.ucs;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.reactivex.disposables.Disposable;
import it.cnr.iit.multiresourcehandler.ComponentsWrapper;
import it.cnr.iit.multiresourcehandler.db.EndAccessSessions;
import it.cnr.iit.ucs.message.endaccess.EndAccessMessage;
import it.cnr.iit.ucs.message.endaccess.EndAccessResponseMessage;
import it.cnr.iit.ucs.message.startaccess.StartAccessMessage;
import it.cnr.iit.ucs.message.startaccess.StartAccessResponseMessage;
import it.cnr.iit.ucs.message.tryaccess.TryAccessMessage;
import it.cnr.iit.ucs.message.tryaccess.TryAccessResponseMessage;
import it.cnr.iit.ucs.properties.components.PepProperties;

@Component
public class UcsClient {

	@Autowired
	private ComponentsWrapper componentsWrapper;

	private static final Logger log = Logger.getLogger(UcsClient.class.getName());

	List<Disposable> dList = new ArrayList<>();

	public TryAccessResponseMessage tryAccess(String request, String policy) {
		TryAccessMessage message = buildTryAccessMessage(request, policy);
		TryAccessResponseMessage response = (TryAccessResponseMessage) componentsWrapper.getUcs().tryAccess(message);
		return response;
	}

	public StartAccessResponseMessage startAccess(String sessionId) {
		StartAccessMessage message = buildStartAccessMessage(sessionId);
		StartAccessResponseMessage response = (StartAccessResponseMessage) componentsWrapper.getUcs()
				.startAccess(message);
		return response;
	}

	public EndAccessResponseMessage endAccess(String sessionId_ucs, String sessionId_eh) {
		EndAccessMessage message = buildEndAccessMessage(sessionId_ucs);
		EndAccessSessions endAccessSessions = new EndAccessSessions();
		endAccessSessions.setMessage_id(message.getMessageId());
		endAccessSessions.setSession_id_ucs(sessionId_ucs);
		endAccessSessions.setSession_id_eh(sessionId_eh);
		componentsWrapper.getEndAccessStorage().createOrUpdate(endAccessSessions);
		EndAccessResponseMessage response = (EndAccessResponseMessage) componentsWrapper.getUcs().endAccess(message);
		return response;
	}

	private TryAccessMessage buildTryAccessMessage(String request, String policy) {
		// FIXME getting only one pep here but we have a list instead
		PepProperties pepProperties = componentsWrapper.getProperties().getPepList().get(0);
		TryAccessMessage message = new TryAccessMessage(pepProperties.getId(), pepProperties.getUri());
		message.setRequest(request);
		message.setPolicy(policy);
		return message;
	}

	private StartAccessMessage buildStartAccessMessage(String sessionId) {
		// FIXME getting only one pep here but we have a list instead
		PepProperties pepProperties = componentsWrapper.getProperties().getPepList().get(0);
		StartAccessMessage message = new StartAccessMessage(pepProperties.getId(), pepProperties.getUri());
		message.setSessionId(sessionId);
		return message;
	}

	private EndAccessMessage buildEndAccessMessage(String sessionId) {
		// FIXME getting only one pep here but we have a list instead
		PepProperties pepProperties = componentsWrapper.getProperties().getPepList().get(0);
		EndAccessMessage message = new EndAccessMessage(pepProperties.getId(), pepProperties.getUri());
		message.setSessionId(sessionId);
		return message;
	}

}
