package it.cnr.iit.mrhtest;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Optional;
import java.util.logging.Logger;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.wso2.balana.utils.Constants.PolicyConstants.DataType;

import it.cnr.iit.clients.dpos.DpoMetadata;
import it.cnr.iit.clients.dpos.DposClient;
import it.cnr.iit.multiresourcehandler.XacmlUtils;
import it.cnr.iit.multiresourcehandler.deployer.MultiResourceHandlerDeployer;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MultiResourceHandlerDeployer.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class TestXacmlUtils extends BaseTest {

	private Logger log = Logger.getLogger(TestXacmlUtils.class.getName());

	@Autowired
	private DposClient dposClient;

	@Test
	public void testFilterRuleIdPrefix() {
		log.info("Test removing all rules with ruleId starting with OBLIGATION");
		String policy = getFileContents("policy1.xml");
		policy = XacmlUtils.filterRuleIdPrefix(policy, "OBLIGATION").orElse("");
		assertTrue(policy.contains("RuleId=\"AUTHORIZATION"));
		assertFalse(policy.contains("RuleId=\"OBLIGATION"));
	}

	@Test
	public void testFilterTwoRuleIdPrefixes() {
		log.info("Test removing all rules with ruleId starting with AUTHORIZATION or PROHIBITION");
		String policy = getFileContents("policy1.xml");
		policy = XacmlUtils.filterRuleIdPrefix(policy, "AUTHORIZATION", "PROHIBITION").orElse("");
		assertTrue(policy.contains("RuleId=\"OBLIGATION"));
		assertFalse(policy.contains("RuleId=\"AUTHORIZATION"));
		assertFalse(policy.contains("RuleId=\"PROHIBITION"));
	}

	@Test
	public void testTranslateDpoMetadataWithMockedDpos() {
		testTranslateDpoMetadata(getMockedDpos());
	}

	@Test
//	@Ignore("Test could easily fail")
	public void testTranslateDpoMetadataWithRealDpos() {
		testTranslateDpoMetadata(dposClient);
	}

	public void testTranslateDpoMetadata(DposClient dposUtils) {
		log.info("Test request dpo-metadata translation");
		String request = getFileContents("request1.xml");
		XacmlUtils.setDposUtils(dposUtils);
		request = XacmlUtils.translateDpoMetadata(request).orElse("");
		log.info(request);
		assertFalse(request.contains(XacmlUtils.dpoMetadataAttributeId));
		assertTrue(request.contains(XacmlUtils.resStartDateAttributeId));
		assertTrue(request.contains(XacmlUtils.resEndDateAttributeId));
		assertTrue(request.contains(XacmlUtils.resStartTimeAttributeId));
		assertTrue(request.contains(XacmlUtils.resEndTimeAttributeId));
		assertTrue(request.contains(XacmlUtils.resOwnerAttributeId));
		assertTrue(request.contains(XacmlUtils.resTypeAttributeId));
		assertTrue(request.contains(DataType.DATE));
		assertTrue(request.contains(DataType.TIME));
	}

	public DposClient getMockedDpos() {
		DposClient dposClient = Mockito.mock(DposClient.class);
		String json = "{\"start_time\":\"2019-10-29T13:35:15Z\",\"event_type\":\"mrhtest-do-not-delete\",\"organization\":\"SPARTACompany1\",\"end_time\":\"2019-10-29T14:35:15Z\",\"id\":\"1573648244615-7bb5aa45-14cf-4986-a308-e6d689e4f1eb\",\"dsa_id\":\"DSA-f93cc71f-6c47-46ec-8281-dc91487dd4f8\"}";
		Mockito.when(dposClient.getDpoMetadata(Mockito.anyString()))
				.thenReturn(Optional.of(DpoMetadata.fromJson(json)));
		return dposClient;
	}
}
