package it.cnr.iit.mrhtest;

import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.io.FileUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import it.cnr.iit.clients.dpos.DposClient;
import it.cnr.iit.clients.dsamapper.DsaMapperClient;
import it.cnr.iit.multiresourcehandler.UcsUtils;
import it.cnr.iit.multiresourcehandler.db.DpoInfo;
import it.cnr.iit.multiresourcehandler.decisioncombiner.DecisionCombiner;
import it.cnr.iit.multiresourcehandler.decisioncombiner.RandomlyAllowHalfDecisionCombiner;
import it.cnr.iit.multiresourcehandler.deployer.MultiResourceHandlerDeployer;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MultiResourceHandlerDeployer.class)
public class TestDecisionCombiner extends BaseTest {

	private Logger log = Logger.getLogger(TestDecisionCombiner.class.getName());

	@Autowired
	private DsaMapperClient dsaMapper;

	@Autowired
	private DecisionCombiner<DpoInfo> decisionCombiner;

	@Autowired
	private DposClient dpos;

	private static final String requestFile = "request_template.xml";

	protected Path tempPath;

	public TestDecisionCombiner() throws IOException {
		tempPath = createTmpPath();
		deleteOnExit(tempPath);
	}

	@Test
	public void testRandomlyAllowHalfStubDecisionCombiner() {
		DecisionCombiner<Integer> decisionCombiner = new RandomlyAllowHalfDecisionCombiner<>();

		List<Integer> origList = IntStream.of(1, 2, 3, 4).boxed().collect(Collectors.toList());
		List<Integer> list = decisionCombiner.getAllowed(origList);
		log.info("list : " + Arrays.toString(list.toArray()));
		assertTrue(list.size() == 2);

		List<Integer> notInList = new ArrayList<>(origList);
		notInList.removeAll(list);
		log.info("notInList : " + Arrays.toString(notInList.toArray()));
		assertTrue(list.size() == 2);

		list = decisionCombiner.getAllowed(IntStream.of(1, 2, 3, 4, 5).boxed().collect(Collectors.toList()));
		log.info("list : " + Arrays.toString(list.toArray()));
		assertTrue(list.size() == 3);

		list = decisionCombiner.getAllowed(IntStream.of(1).boxed().collect(Collectors.toList()));
		log.info("list : " + Arrays.toString(list.toArray()));
		assertTrue(list.size() == 1);
	}

	@Test
	@Ignore
	public void testDecisionCombiner() throws FileNotFoundException {
		List<String> dsaList = Arrays.asList("DSA-b78f399a-067c-41df-a7ce-0414585bc605",
				"DSA-faa89ca6-0523-49d4-b5ab-8939088e79af", "DSA-8c042c14-79db-4bb2-ba07-53d44063cfb3");

		List<String> dpoList = Arrays.asList("1572444059088-ecc94679-c6ab-41c6-a5e5-ae0b48b0e2ca",
				"1572444060788-f5c30b72-8c9d-4cdb-a16f-f8746f6a2339",
				"1572444062135-97fdedc7-bdc8-4fdb-86d8-eafa949e0473",
				"1572444063472-4eb90395-f79c-4bfe-be9a-611ca3320870",
				"1572444064916-a71cdcea-dab3-42bf-a7bd-070a229edb3a",
				"1572444066344-f334260e-0297-40d8-b5a6-5617388b8e39",
				"1572444067821-e3909db9-c073-44cb-b8fe-09309313a238",
				"1572444069170-ea54200a-fa58-4456-a6fb-c69d200c1cd6");

		String actionId = "invokebruteforceattacksdetection";
		String user = "user";
		String accessPurpose = "Cyber Threat Monitoring";
		List<DpoInfo> initialDpoInfoList = Arrays.asList(
				prepareDpoInfo(requestFile, "ISP", actionId, user, "IT", "DataAnalyzer", "ResearchDepartment", "A",
						accessPurpose, dpoList.get(0), dsaList.get(0)),
				prepareDpoInfo(requestFile, "ISP", actionId, user, "IT", "DataAnalyzer", "ResearchDepartment", "B",
						accessPurpose, dpoList.get(3), dsaList.get(1)),
				prepareDpoInfo(requestFile, "ISP", actionId, user, "IT", "DataAnalyzer", "ResearchDepartment", "C",
						accessPurpose, dpoList.get(7), dsaList.get(2)));
		List<DpoInfo> expectedDpoInfoList = new ArrayList<>(initialDpoInfoList);
		expectedDpoInfoList.removeIf(c -> c.getDpoId().equals(dpoList.get(0)));

		List<DpoInfo> dpoInfoList = decisionCombiner.getAllowed(initialDpoInfoList);
		log.info("original dpo list : " + Arrays
				.toString(initialDpoInfoList.stream().map(DpoInfo::getDpoId).collect(Collectors.toList()).toArray()));
		log.info("expected dpo list : " + Arrays
				.toString(expectedDpoInfoList.stream().map(DpoInfo::getDpoId).collect(Collectors.toList()).toArray()));
		log.info("selected dpo list : "
				+ Arrays.toString(dpoInfoList.stream().map(DpoInfo::getDpoId).collect(Collectors.toList()).toArray()));

		assertTrue(expectedDpoInfoList.equals(dpoInfoList));
	}

	@Test
	@Ignore
	public void testDecisionCombinerSparta() throws FileNotFoundException {
		List<String> dsaListSparta = Arrays.asList("DSA-f93cc71f-6c47-46ec-8281-dc91487dd4f8",
				"DSA-ad255ba9-07ee-4907-b090-9e90299116f7", "DSA-93d15379-92d2-454e-a5bc-865dd06c4003");

		List<String> dpoListSparta = Arrays.asList("1575382059824-6480ea39-9811-4ffd-b98b-ceef0e21b40f",
				"1575382061370-24939896-e93b-4e45-a0f4-708cfcaf83a9",
				"1575382062574-f75b972b-e7fc-4610-a3d7-f8b13175267b",
				"1575382063827-33c8afc3-d23f-416b-9770-8442794e0879",
				"1575382064993-67f0e02f-9eaa-483d-a334-5f0f1464e1cc",
				"1575382066191-ce1eaf2d-5e83-4ba3-adc5-a1a79602defd");

		String actionId = "invokespamemaildetect";
		String accessPurpose = "Cyber Threat Monitoring";
		List<DpoInfo> initialDpoInfoList = Arrays.asList(
				prepareDpoInfo(requestFile, "SPARTA", actionId, "SpartaUser1", "IT", "DataAnalyzer",
						"SPARTADepartment1", "SpartaCompany1", accessPurpose, dpoListSparta.get(0),
						dsaListSparta.get(0)),
				prepareDpoInfo(requestFile, "SPARTA", actionId, "SpartaUser1", "IT", "DataAnalyzer",
						"SPARTADepartment1", "SpartaCompany1", accessPurpose, dpoListSparta.get(1),
						dsaListSparta.get(0)),
				prepareDpoInfo(requestFile, "SPARTA", actionId, "SpartaUser2", "UK", "DataAnalyzer",
						"SPARTADepartment2", "SpartaCompany2", accessPurpose, dpoListSparta.get(2),
						dsaListSparta.get(1)),
				prepareDpoInfo(requestFile, "SPARTA", actionId, "SpartaUser2", "UK", "DataAnalyzer",
						"SPARTADepartment2", "SpartaCompany2", accessPurpose, dpoListSparta.get(3),
						dsaListSparta.get(1)),
				prepareDpoInfo(requestFile, "SPARTA", actionId, "SpartaUser3", "DE", "DataAnalyzer",
						"SPARTADepartment3", "SpartaCompany3", accessPurpose, dpoListSparta.get(4),
						dsaListSparta.get(2)),
				prepareDpoInfo(requestFile, "SPARTA", actionId, "SpartaUser3", "DE", "DataAnalyzer",
						"SPARTADepartment3", "SpartaCompany3", accessPurpose, dpoListSparta.get(5),
						dsaListSparta.get(2)));
		List<DpoInfo> expectedDpoInfoList = new ArrayList<>(initialDpoInfoList);
		expectedDpoInfoList.removeIf(c -> c.getDpoId().equals(dpoListSparta.get(4)));
		expectedDpoInfoList.removeIf(c -> c.getDpoId().equals(dpoListSparta.get(5)));

		List<DpoInfo> dpoInfoList = decisionCombiner.getAllowed(initialDpoInfoList);
		log.info("original dpo list : " + Arrays
				.toString(initialDpoInfoList.stream().map(DpoInfo::getDpoId).collect(Collectors.toList()).toArray()));
		log.info("expected dpo list : " + Arrays
				.toString(expectedDpoInfoList.stream().map(DpoInfo::getDpoId).collect(Collectors.toList()).toArray()));
		log.info("selected dpo list : "
				+ Arrays.toString(dpoInfoList.stream().map(DpoInfo::getDpoId).collect(Collectors.toList()).toArray()));

		assertTrue(expectedDpoInfoList.equals(dpoInfoList));
	}

	private DpoInfo prepareDpoInfo(String requestPath, String pilot, String actionId, String user, String country,
			String role, String memberOf, String organisation, String accessPurpose, String dpoId, String dsaId)
			throws FileNotFoundException {
		String request = getFileContents(requestPath);
		String filledRequest = UcsUtils.prepareRequest(request.replace("[%PILOT%]", pilot)
				.replace("[%ACTION_ID%]", actionId).replace("[%USER%]", user).replace("[%ORGANISATION%]", organisation)
				.replace("[%ACCESS_PURPOSE%]", accessPurpose).replace("[%DPO_ID%]", dpoId).replace("[%DSA_ID%]", dsaId)
				.replace("[%COUNTRY%]", country).replace("[%ROLE%]", role).replace("[%MEMBEROF%]", memberOf));
		writeTmpFile(dpoId + ".xml", filledRequest);
		DpoInfo dpoInfo = new DpoInfo();
		dpoInfo.setDpoId(dpoId);
		dpoInfo.setRequestPath(getTmpFilePath(dpoId + ".xml").toString());
		dpos.getDpoMetadata(dpoId).ifPresent(m -> dpoInfo.setDpoMetadata(m));
		return dpoInfo;
	}

	@Test
	public void testDecisionCombinerUtils() {
		String otherDataPolicy = dsaMapper.getOtherDataPolicy("DSA-287bfedb-09f3-4002-9dbd-9dba792ed042").get();
		log.info("otherDataPolicy : " + otherDataPolicy);
		assertTrue(otherDataPolicy.contains("<upol:Policy"));
	}

	protected Path getTmpFilePath(String file) {
		return Paths.get(tempPath.toString(), file);
	}

	protected void writeTmpFile(String fileName, String content) throws FileNotFoundException {
		try (PrintStream out = new PrintStream(new FileOutputStream(getTmpFilePath(fileName).toString()))) {
			out.print(content);
		}
	}

	Path createTmpPath() throws IOException {
		return Files.createTempDirectory(UUID.randomUUID().toString());
	}

	void deleteOnExit(Path path) {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				try {
					FileUtils.deleteDirectory(path.toFile());
				} catch (IOException ex) {
				}
			}
		});
	}
}
