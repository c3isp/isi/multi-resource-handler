package it.cnr.iit.mrhtest;

import static org.junit.Assert.fail;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import it.cnr.iit.ucs.core.UCSCoreServiceBuilder;
import it.cnr.iit.ucs.pdp.PolicyDecisionPoint;
import it.cnr.iit.ucs.properties.UCSProperties;
import it.cnr.iit.ucs.ucs.UCSInterface;

public abstract class BaseTest {

	@Autowired
	public UCSProperties properties;

	public UCSInterface ucs;

	public PolicyDecisionPoint pdp;

	@PostConstruct
	private void init() {
		ucs = new UCSCoreServiceBuilder().setProperties(properties).build();
		pdp = new PolicyDecisionPoint(properties.getPolicyDecisionPoint());
	}

	protected String getFileContents(String file) {
		try {
			Path path = getFilePath(file);
			return new String(Files.readAllBytes(path));
		} catch (IOException e) {
			fail("error loading file");
		}
		return "";
	}

	protected static Path getFilePath(String file) {
		return Paths.get(TestMultiResourceHandler.class.getClassLoader().getResource(file).getPath());
	}

}
