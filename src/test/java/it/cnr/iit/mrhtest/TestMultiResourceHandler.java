package it.cnr.iit.mrhtest;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.IntStream;

import javax.xml.bind.JAXBException;

import org.awaitility.Awaitility;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Primary;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.google.gson.Gson;

import io.reactivex.SingleObserver;
import it.cnr.iit.clients.ucs.UcsClient;
import it.cnr.iit.common.eventhandler.events.multiresourcehandler.MultiResourceHandlerEvent;
import it.cnr.iit.common.eventhandler.events.multiresourcehandler.MultiResourceHandlerEventTypes;
import it.cnr.iit.multiresourcehandler.MultiResourceHandler;
import it.cnr.iit.multiresourcehandler.UcsUtils;
import it.cnr.iit.multiresourcehandler.db.DPO_STATUS;
import it.cnr.iit.multiresourcehandler.db.DpoInfo;
import it.cnr.iit.multiresourcehandler.db.DpoInfoStorage;
import it.cnr.iit.multiresourcehandler.deployer.MultiResourceHandlerDeployer;
import it.cnr.iit.ucs.message.Message;
import it.cnr.iit.ucs.message.startaccess.StartAccessResponseMessage;
import it.cnr.iit.ucs.message.tryaccess.TryAccessResponseMessage;
import it.cnr.iit.ucs.pdp.PDPEvaluation;
import it.cnr.iit.ucs.pdp.PDPResponse;
import oasis.names.tc.xacml.core.schema.wd_17.DecisionType;
import oasis.names.tc.xacml.core.schema.wd_17.ResponseType;
import oasis.names.tc.xacml.core.schema.wd_17.ResultType;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MultiResourceHandlerDeployer.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@ComponentScan("it.cnr.iit")
public class TestMultiResourceHandler {

	@Autowired
	private MultiResourceHandler mrh;

	@Autowired
	private DpoInfoStorage dpoInfoStorage;

	private static Map<String, SingleObserver<? extends Message>> observerMap;

	private static final String PERMIT = "<Response xmlns=\"urn:oasis:names:tc:xacml:3.0:core:schema:wd-17\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"urn:oasis:names:tc:xacml:3.0:core:schema:wd-17 http://docs.oasis-open.org/xacml/3.0/xacml-core-v3-schema-wd-17.xsd\">"
			+ "<Result>" + " <Decision>Permit</Decision>" + "</Result>" + "</Response>";
	private static final String DENY = "<Response xmlns=\"urn:oasis:names:tc:xacml:3.0:core:schema:wd-17\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"urn:oasis:names:tc:xacml:3.0:core:schema:wd-17 http://docs.oasis-open.org/xacml/3.0/xacml-core-v3-schema-wd-17.xsd\">"
			+ "<Result>" + " <Decision>Deny</Decision>" + "</Result>" + "</Response>";

	private static final int TIMEOUT = 2;

	@Before
	public void init() {
		observerMap = new HashMap<>();
	}

	@TestConfiguration
	static class TestConfig {
		@Bean
		@Primary
		public UcsClient getUcsClient() {
			UcsClient ucs = Mockito.mock(UcsClient.class);

//			Mockito.when(ucs.enrichRequest(Mockito.anyString(), Mockito.anyString())).then(new Answer<String>() {
			Mockito.when("").then(new Answer<String>() {
				@Override
				public String answer(InvocationOnMock invocation) throws Throwable {
					return invocation.getArgument(0, String.class);
				}
			});

			Mockito.doAnswer((Answer<?>) invocation -> {
				SingleObserver<? extends Message> observer = invocation.getArgument(0);
				observerMap.put(UUID.randomUUID().toString(), observer);
				return null;
			}).when(ucs).tryAccess(Mockito.any(), Mockito.anyString());

			Mockito.doAnswer((Answer<?>) invocation -> {
				SingleObserver<? extends Message> observer = invocation.getArgument(0);
				observerMap.put(UUID.randomUUID().toString(), observer);
				return null;
			}).when(ucs).startAccess(Mockito.any());

			Mockito.doAnswer((Answer<?>) invocation -> {
				SingleObserver<? extends Message> observer = invocation.getArgument(0);
				observerMap.put(UUID.randomUUID().toString(), observer);
				return null;
			}).when(ucs).endAccess(Mockito.any(), Mockito.anyString());

//			Mockito.doAnswer((Answer<?>) invocation -> {
//				Message message = invocation.getArgument(0);
//				@SuppressWarnings("unchecked")
//				SingleObserver<Message> observer = (SingleObserver<Message>) observerMap.remove(message.getMessageId());
//				observer.onSuccess(message);
//				return null;
//			}).when(ucs).receive(Mockito.any());

			return ucs;
		}
	}

	@Test
	public void multiTryAccessSentTest10() {
		multiTryAccessSentTest(10);
	}

	@Test
	public void multiTryAccessSentTest1() {
		multiTryAccessSentTest(1);
	}

	@Test
	public void multiTryAccessSentTest0() {
		multiTryAccessSentTest(0);
	}

	@Test
	public void multiTryAccessSentAndReply10() {
		multiTryAccessSentAndReply(10);
	}

	@Test
	public void multiTryAccessSentAndReply1() {
		multiTryAccessSentAndReply(1);
	}

	public void multiTryAccessSentTest(int size) {
		MultiResourceHandlerEvent event = createTryAccessMultiEvent(size);
		mrh.handleEvent(event);
		assertTrue(dpoInfoStorage.getForSessionIdEh(event.getSessionID()).size() == size);
	}

	public void multiTryAccessSentAndReply(int size) {
		MultiResourceHandlerEvent event = createTryAccessMultiEvent(size);

		mrh.handleEvent(event);

		List<DpoInfo> list = dpoInfoStorage.getForSessionIdEh(event.getSessionID());
		assertTrue(list.size() == size);

		list.stream().forEach(d -> assertTrue(d.isStatus(DPO_STATUS.TRYACCESS_PENDING)));
		IntStream.range(0, size).forEach(i -> {
			String messageId = observerMap.keySet().stream().findFirst().get();
			Message response = createTryAccessResponse(messageId);
//			ucs.receive(response);
		});

		list = dpoInfoStorage.getForSessionIdEhAndStatus(event.getSessionID(), DPO_STATUS.STARTACCESS_PENDING);
		assertTrue(list.size() == size);

		dpoInfoStorage.getForSessionIdEh(event.getSessionID()).stream().forEach(d -> {
			Awaitility.await().atMost(TIMEOUT, TimeUnit.SECONDS).untilTrue(
					new AtomicBoolean(d.isStatus(DPO_STATUS.STARTACCESS_PENDING) && d.getSessionIdUcs().length() > 0));
		});

		list = dpoInfoStorage.getForSessionIdEhAndStatus(event.getSessionID(), DPO_STATUS.STARTACCESS_PENDING);
		assertTrue(list.size() == size);

		IntStream.range(0, size).forEach(i -> {
			String messageId = observerMap.keySet().stream().findFirst().get();
			Message response = createStartAccessResponse(messageId);
//			ucs.receive(response);
		});

		dpoInfoStorage.getForSessionIdEh(event.getSessionID()).stream().forEach(d -> {
			Awaitility.await().atMost(TIMEOUT, TimeUnit.SECONDS)
					.untilTrue(new AtomicBoolean(d.isStatus(DPO_STATUS.STARTACCESS_PERMIT)));
		});

		list = dpoInfoStorage.getForSessionIdEhAndStatus(event.getSessionID(), DPO_STATUS.STARTACCESS_PERMIT);
		assertTrue(list.size() == size);

		MultiResourceHandlerEvent endEvent = createEndAccessEvent(event.getSessionID());
		mrh.handleEvent(endEvent);

		list = dpoInfoStorage.getForSessionIdEhAndStatus(event.getSessionID(), DPO_STATUS.STARTACCESS_PERMIT);
		assertTrue(list.size() == 0);
	}

	private static MultiResourceHandlerEvent createTryAccessMultiEvent(int size) {
		MultiResourceHandlerEvent event = new MultiResourceHandlerEvent();
		event.setMessageID(UUID.randomUUID().toString());
		event.setSessionID(UUID.randomUUID().toString());
		event.setEventType(MultiResourceHandlerEventTypes.TRY_ACCESS_MULTI);
		String policy = TestMultiResourceHandler.class.getClassLoader().getResource("policy1.xml").getPath();
		String request = TestMultiResourceHandler.class.getClassLoader().getResource("request1.xml").getPath();
		String[] policies = new String[size];
		String[] requests = new String[size];
		for (int i = 0; i < size; i++) {
			policies[i] = policy;
			requests[i] = request;
		}
		event.getAdditionalProperties().put(UcsUtils.POLICY_KEY, new Gson().toJson(policies));
		event.getAdditionalProperties().put(UcsUtils.REQUEST_KEY, new Gson().toJson(requests));

		return event;
	}

	private static MultiResourceHandlerEvent createEndAccessEvent(String sessionId) {
		MultiResourceHandlerEvent event = new MultiResourceHandlerEvent();
		event.setSessionID(sessionId);
		event.setEventType(MultiResourceHandlerEventTypes.END_ACCESS);
		return event;
	}

	private static Message createTryAccessResponse(String id) {
		TryAccessResponseMessage tryAccessResponse = new TryAccessResponseMessage("localhost", "localhost", id);
		tryAccessResponse.setSessionId(UUID.randomUUID().toString());
		tryAccessResponse.setMessageId(id);
		try {
			tryAccessResponse.setEvaluation((PDPEvaluation) pdpEvaluation(true).getResponseType());
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return tryAccessResponse;
	}

	private Message createStartAccessResponse(String id) {
		StartAccessResponseMessage message = new StartAccessResponseMessage("localhost", "localhost", id);
		try {
			message.setEvaluation((PDPEvaluation) pdpEvaluation(true).getResponseType());
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return message;
	}

	private static PDPResponse pdpEvaluation(boolean permit) throws JAXBException {
		ResponseType response = new ResponseType();
		ResultType result = new ResultType();
		List<ResultType> resultList = new ArrayList<ResultType>();
		result.setDecision(permit ? DecisionType.PERMIT : DecisionType.DENY);
		resultList.add(result);
		response.setResult(resultList);
		return new PDPResponse(response);
//		return new PDPResponse(permit ? PERMIT : DENY);
	}

}
