package it.cnr.iit.mrhtest;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import org.junit.Test;

import it.cnr.iit.multiresourcehandler.UcsUtils;
import it.cnr.iit.ucs.constants.STATUS;
import it.cnr.iit.ucs.exceptions.PolicyException;
import it.cnr.iit.ucs.exceptions.RequestException;
import it.cnr.iit.ucs.pdp.PDPResponse;
import it.cnr.iit.xacml.wrappers.PolicyWrapper;
import it.cnr.iit.xacml.wrappers.RequestWrapper;

public class TestCustomPDP extends BaseTest {

	private Logger LOG = Logger.getLogger(TestCustomPDP.class.getName());

	@Test
	public void test_getTriggeredObligationRuleIdList() {
		String policy = UcsUtils.preparePolicyForObligation(getFileContents("policy2.xml"));
		String request = UcsUtils.prepareRequest(getFileContents("request2.xml"));
		List<String> expectedObligationList = Arrays.asList("OBLIGATION_Expiration", "default-deny");
		List<String> obligationList = UcsUtils.getTriggeredObligationRuleIdList(request, policy);
		LOG.info("Expected obligations : " + Arrays.toString(expectedObligationList.toArray()));
		LOG.info("Returned obligations : " + Arrays.toString(obligationList.toArray()));
		assertTrue(expectedObligationList.equals(obligationList));
	}

	@Test
	public void test_pdpReturnsReturnsCorrectObligations() {
		List<String> expectedObligationList = Arrays.asList("AUTHORIZATION_7", "OBLIGATION_36");
		testPDPWithObligations("request1.xml", "policy1.xml", "permit", expectedObligationList);
	}

	@Test
	public void test_pdpReturnsDenyOtherData() {
		List<String> expectedObligationList = Arrays.asList("PROHIBITION_1");
		testPDPWithObligations("request_other_data.xml", "policy_other_data.xml", "deny", expectedObligationList);
	}

	@Test
	public void test_pdpReturnsPermitMember() {
		List<String> expectedObligationList = Arrays.asList("AUTHORIZATION_19");
		testPDPWithObligations("requestMember.xml", "policyMember.xml", "permit", expectedObligationList);
	}

	@Test
	public void test_pdpReturnsPermit() {
		List<String> expectedObligationList = Arrays.asList("AUTHORIZATION_8");
		testPDPWithObligations("request2.xml", "policy2.xml", "permit", expectedObligationList);
	}

	@Test
	public void test_pdpReturnsDeny() {
		List<String> expectedObligationList = Arrays.asList("PROHIBITION_DsaSanityCheck", "OBLIGATION_Expiration",
				"default-deny");
		testPDPWithObligations("request2.xml", "policy_deny.xml", "deny", expectedObligationList);
	}

	private PDPResponse testPDPWithObligations(String requestFile, String policyFile, String expectedResponse,
			List<String> expectedObligationList) {
		PDPResponse response = testPDP(requestFile, policyFile, expectedResponse);
		List<String> obligationList = response.getObligations();
		LOG.info("Testing : " + requestFile + ", " + policyFile);
		LOG.info("Expected obligations : " + Arrays.toString(expectedObligationList.toArray()));
		LOG.info("Returned obligations : " + Arrays.toString(obligationList.toArray()));
		assert (expectedObligationList.equals(obligationList));
		return response;
	}

	private PDPResponse testPDP(String requestFile, String policyFile, String expectedResponse) {
		String policy = UcsUtils.preparePolicy(getFileContents(policyFile));
		String request = UcsUtils.prepareRequest(getFileContents(requestFile));
		System.out.println(request);
		PDPResponse response = null;
		try {
			response = (PDPResponse) pdp.evaluate(RequestWrapper.build(request), PolicyWrapper.build(policy),
					STATUS.TRY);
		} catch (RequestException | PolicyException e) {
			e.printStackTrace();
		}
		assertTrue(!response.getResponse().isEmpty());
		assertTrue(response.getResponse().equalsIgnoreCase(expectedResponse));
		return response;
	}

}
