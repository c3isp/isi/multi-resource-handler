package it.cnr.iit.mrhtest;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import it.cnr.iit.multiresourcehandler.db.DpoInfo;
import it.cnr.iit.multiresourcehandler.db.DpoInfoStorage;
import it.cnr.iit.multiresourcehandler.deployer.MultiResourceHandlerDeployer;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MultiResourceHandlerDeployer.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class TestDpoInfoStorage {

	@Autowired
	DpoInfoStorage dpoInfoStorage;

	@Test
	public void testInsert() {
		assertTrue(addDpoInfo("123", "123", "123", "123"));
		assertTrue(dpoInfoStorage.getForDpoId("123") != null);
	}

	public boolean addDpoInfo(String dpoId, String policyPath, String requestPath, String sessionIdEh) {
		DpoInfo dpoInfo = new DpoInfo();
		dpoInfo.setDpoId(dpoId);
		dpoInfo.setSessionIdEh(sessionIdEh);
		dpoInfo.setRequestPath(requestPath);
		dpoInfo.setPolicyPath(policyPath);
		return dpoInfoStorage.createOrUpdate(dpoInfo);
	}
}
